<?php
spl_autoload_register(
    function ($class) 
    {
        $baseDir = __DIR__ . '/';
        
        // get the class file name
        $classParts = explode('\\', $class);
        $className = array_pop($classParts);
        $namespace = implode('/', $classParts);

        $classFileName = '';
        preg_match_all('/[A-Z]+[^A-Z]+/', $className, $matches);
        foreach ($matches[0] as $key => $value) 
        {
            $classFileName .= $value . '_';
        }
        $classFileName = strtolower(rtrim($classFileName, '_'));
        
        // replace the namespace prefix with the base directory, replace namespace
        // separators with directory separators in the relative class name, append
        // with .php
        $file = strtolower($baseDir . $namespace . '/' . str_replace('\\', '/', $classFileName) . '.php');
        
        // if the file exists, include it
        if (file_exists($file)) 
        {
            include $file;
        }
    }
);
