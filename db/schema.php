<?php
namespace Migration;

class Schema extends RecordSchema
{
    protected function define()
    {
        createTable(
            'users', 
            [
                'columns' => [
                    'string' => 'profileName',
                    'datetime' => 'createdAt',
                    'datetime' => 'updatedAt',
                    'string' => 'password'
                ]
            ]
        );        
    }
}