<?php
namespace Migration;

class CreateProfiles extends RecordMigration
{
    protected function change()
    {
        createTable('users', [
            'columns' => [
                'string' => 'profileName'
            ],
            'timestamps' => true
        ]);
    }
}
