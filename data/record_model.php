<?php
namespace Data;

abstract class RecordModel
{
    private static $_queryable;
    private static $_queryChain = [];

    public static function find($options)
    {        
        self::_prepareQueryable();
        return self::$_queryable->find(self::_getCalledClassName(), $options);
    }

    public static function take($count = 1)
    {
        self::_prepareQueryable();
        return self::$_queryable->take(self::_getCalledClassName(), $count);
    }

    public static function first($count = 1)
    {
        self::_prepareQueryable();
        return self::$_queryable->first(self::_getCalledClassName(), $count);
    }

    public static function last($count = 1)
    {

    }

    public static function findBy($options)
    {

    }

    public static function all()
    {
        self::_prepareQueryable();
        return self::$_queryable->all(self::_getCalledClassName());
    }

    public static function where($options = null)
    {
        self::_prepareQueryable();
        array_push(self::$_queryChain, ['where' => $options]);
        if ($options == null)
        {
            return self::$_queryable;
        }        

        return self::$_queryable->where(self::_getCalledClassName(), $options);
    }

    public static function not($options)
    {
        self::_prepareQueryable();
        
        return self::$_queryable->not(self::_getCalledClassName(), $options);
    }

    public static function order($options)
    {

    }

    public static function select($options)
    {

    }

    public static function distinct()
    {

    }

    public static function limit($count)
    {

    }

    public static function offset($count)
    {

    }

    public static function group($options)
    {

    }

    public static function joins($options)
    {

    }

    // private methods
    private static function _prepareQueryable()
    {
        $connection = \DAL\ApplicationContext::openConnection();
        if (self::$_queryable == null)
        {
            self::$_queryable = $connection->getQueryable();
        }
    }

    private static function _getCalledClassName()
    {
        return array_pop(explode('\\', get_called_class()));
    }
}
