<?php
namespace Data;

class RecordSet implements \Data\IRecordQueryable
{
    private $_connection;
    private $_model;
    private $_queryBuilder;

    private $_queryMap = [];

    public function __construct(\Data\DatabaseConnection $connection, string $model)
    {
        $this->_connection = $connection;
        $this->_model = $model;

        $this->_queryBuilder = $connection->getQueryBuilder();
        
        // build the initial query map
        $this->_initQueryMap();
    }

    public function find($options)
    {
        array_push($this->_queryMap['where'], ['int:id' => $options]);

        $count = (is_array($options) || $options instanceof \Countable) ? count($options) : 1;
        return $this->take($count);
    }

    public function take(int $count = 1)
    {
        $this->_queryMap['limit'] = $count;

        return $this->_go();
    }

    public function first(int $count = 1)
    {
        array_push($this->_queryMap['order'], ['asc' => 'id']);

        return $this->take($count);
    }

    public function last(int $count = 1)
    {
        array_push($this->_queryMap['order'], ['desc' => 'id']);

        return $this->take($count);       
    }

    public function findBy($options)
    {
        array_push($this->_queryMap['where'], $options);

        return $this->take();
    }

    public function all()
    {
        return $this->_go();
    }

    public function where($options = null)
    {
        array_push($this->_queryMap['where'], $options);

        return $this;
    }

    public function not($options)
    {
        array_push($this->_queryMap['not'], $options);
        
        return $this;
    }

    public function order($options)
    {
        array_push($this->_queryMap['order'], $options);
        
        return $this;      
    }

    public function select($options)
    {
        $this->_queryMap['select'] = $options;

        return $this;
    }

    public function distinct()
    {
        $this->_queryMap['distinct'] = true;
        
        return $this;           
    }

    public function limit(int $count)
    {
        $this->_queryMap['limit'] = $count;
        
        return $this;        
    }

    public function offset(int $count)
    {
        $this->_queryMap['offset'] = $count;
        
        return $this;        
    }

    public function group($options)
    {
        
    }

    public function joins($options)
    {
        
    }

    // TODO: Remove need for this function
    public function sql(string $query)
    {
        $result = $this->_connection->executeQuery($query);

        if (is_array($result) && sizeof($result) === 1)
            return array_pop($result);

        return $result;
    }

    // TODO: Remove need for this function
    public function escape($value)
    {
        return $this->_connection->escape($value);
    }

    private function _go()
    {
        $sql = $this->_queryBuilder->buildSelectQuery($this->_queryMap);
        $result = $this->_connection->executeQuery($sql);
        $limit = $this->_queryMap["limit"];

        // when we're done with the query map, reset it
        $this->_initQueryMap();

        if (is_array($result) && sizeof($result) === 1 && $limit === 1)
            return array_pop($result);

        return $result;
    }

    private function _initQueryMap()
    {
        $this->_queryMap = [
            'select' => '*',
            'from' => $this->_model,
            'joins' => [],
            'where' => [],
            'not' => [],
            'group' => [],
            'order' => [],
            'limit' => '',
            'offset' => '',
            'distinct' => false
        ];
    }
}
