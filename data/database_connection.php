<?php
namespace Data;

interface DatabaseConnection
{
    public function open();
    public function executeQuery($query);
    public function escape($query);
    public function getQueryBuilder();
    public function getQueryable();
    public function close();
}
