<?php
namespace Data;

interface IQueryBuilder
{
    // functions for finding records
    public function buildSelectQuery(array $queryMap);

}