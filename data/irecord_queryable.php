<?php
namespace Data;

interface IRecordQueryable
{
    // finder methods
    public function find($options);
    public function take(int $count = 1);
    public function first(int $count = 1);
    public function last(int $count = 1);
    public function findBy($options);
    public function all();
    public function where($options = null);
    public function not($options);
    public function order($options);
    public function select($options);
    public function distinct();
    public function limit(int $count);
    public function offset(int $count);
    public function group($options);
    public function joins($options);

    public function sql(string $query);
}
