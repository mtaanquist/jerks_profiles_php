<?php
namespace Data\Mysql;

class MysqlConnection implements \Data\DatabaseConnection
{
    private $_mysqli;
    private $_connectionSettings;

    function __construct($connectionSettings)
    {
        $this->_connectionSettings = $connectionSettings;
    }

    public function open()
    {
        if ($this->_connectionSettings["ssl"])
        {
            $this->_mysqli = mysqli_init();
            $flags = MYSQLI_CLIENT_SSL_DONT_VERIFY_SERVER_CERT;
            mysqli_ssl_set($this->_mysqli, NULL, NULL, '', NULL, NULL);
        }

        return mysqli_real_connect(
            $this->_mysqli, 
            $this->_connectionSettings["host"], 
            $this->_connectionSettings["username"], 
            $this->_connectionSettings["password"], 
            $this->_connectionSettings["database"], 
            3306, 
            '',
            $flags
        );        
    }

    public function executeQuery($query)
    {
        //print '<pre>' . htmlspecialchars(print_r($query, true)) . '</pre>';
        $result = $this->_mysqli->query($query);
        if ($result === false) 
            throw new \Exception('Database error: ' . $this->_mysqli->error);

        if ($result === true) 
            return true;

        $data = array();
        while ($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
        $result->free();
        return $data;
    }

    public function getQueryBuilder()
    {
        return new \Data\Mysql\MysqlQueryBuilder($this);
    }

    public function getQueryable()
    {
        return new \Data\Mysql\MysqlQueryable($this->getQueryBuilder());
    }

    public function escape($value)
    {
        return $this->_mysqli->real_escape_string($value);
    }

    public function close()
    {
        return $this->_mysqli->close();
    }
}