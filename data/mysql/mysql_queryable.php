<?php
namespace Data\Mysql;

class MysqlQueryable implements \Data\RecordQueryable
{
    private $_queryBuilder;
    private static $_queryChain = [];

    public function __construct($queryBuilder)
    {
        $this->_queryBuilder = $queryBuilder;
    }

    public function find($model, $options)
    {
        if (is_array($options))
        {
            return $this->_findMultiple($model, $options);
        }
        else
        {
            return $this->_findSingle($model, $options);
        }
    }

    private function _findSingle($model, $options)
    {
        return $this->_queryBuilder->buildSelectQuery($model, $options);
    }

    private function _findMultiple($model, Array $options)
    {
        return $this->_queryBuilder->buildSelectQuery($model, (Array)$options);
    }

    public function take($model, $count = 1)
    {

    }

    public function first($model, $count = 1)
    {

    }

    public function last($model, $count = 1)
    {

    }

    public function findBy($model, $options)
    {

    }

    public function all($model)
    {
        return $this->_queryBuilder->buildSelectQuery($model);
    }    
    
    public function where($model, $options = null)
    {
        return $this->_queryBuilder->buildSelectQuery($model, (Array)$options);
    }
    
    public function not($model, $options)
    {
        var_dump(self::$_queryChain);
    }
    
    public function order($model, $options)
    {

    }
    
    public function select($model, $options)
    {

    }
    
    public function distinct($model)
    {

    }
    
    public function limit($model, $count)
    {

    }
    
    public function offset($model, $count)
    {

    }
    
    public function group($model, $options)
    {

    }
    
    public function joins($model, $options)
    {

    }

    public function __toString()
    {
        return $this->_queryChain;
    }
}