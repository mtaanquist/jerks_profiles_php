<?php
namespace Data\Mysql;

class MysqlQueryBuilder implements \Data\IQueryBuilder
{
    private $_connection;

    public function __construct($connection)
    {
        $this->_connection = $connection;
    }

    public function buildSelectQuery(Array $queryMap)
    {
        $sqlLines = [];
        // select
        array_push($sqlLines, "SELECT");
        if ($queryMap['distinct']) array_push($sqlLines, "DISTINCT");
        array_push($sqlLines, $queryMap['select']);

        // from
        array_push($sqlLines, "FROM");
        array_push($sqlLines, $queryMap['from']);

        // where[] and/or not[]
        if (!empty($queryMap['where']))
        {
            array_push($sqlLines, "WHERE");
            $this->_buildWhereSqlLines($queryMap['where'], $sqlLines);
        }

        if (!empty($queryMap['not']))
        {
            array_push($sqlLines, "WHERE");
            $this->_buildWhereSqlLines($queryMap['not'], $sqlLines, false);
        }


        // group

        // order
        if (!empty($queryMap['order']))
        {
            array_push($sqlLines, "ORDER BY");
            $this->_buildOrderSqlLines($queryMap['order'], $sqlLines);
        }

        // limit
        if (!empty($queryMap['limit']))
        {
            $limit = (string)((int)$queryMap['limit']);
            array_push($sqlLines, "LIMIT $limit");
        }

        return implode(' ', $sqlLines);
    }

    private function _buildWhereSqlLines(Array $conditions, Array &$sqlLines, bool $equalTo = true)
    {
        $sqlConditions = [];
        foreach ($conditions as $condition)
        {
            foreach ($condition as $key => $value)
            {               
                $field = explode(':', $key, 2);
                $type = $field[0];
                $name = $field[1];

                switch ($type)
                {
                case 'bool':
                    $sqlValue = ((bool)$value) ? '1' : '0';
                    break;
                case 'double':
                    $sqlValue = (string)((double)$value);
                    break;
                case 'int':
                    if (is_array($value))
                    {
                        $sqlValue = ($equalTo) ? "$name IN (" : "$name NOT IN (";
                        $sqlValueArray = [];
    
                        foreach ($value as $val)
                        {
                            array_push($sqlValueArray, (string)((int)$val));
                        }
                        $sqlValue .= implode(',', $sqlValueArray);
                        $sqlValue .= ")";                        
                    }
                    else
                    {
                        $sqlValue = (string)((int)$value);
                    }
                    break;            
                case 'string':
                    if (is_array($value))
                    {
                        $sqlValue = ($equalTo) ? "$name IN (" : "$name NOT IN (";
                        $sqlValueArray = [];
    
                        foreach ($value as $val)
                        {
                            array_push($sqlValueArray, '"' . $this->_connection->escape($val) . '"');
                        }
                        $sqlValue .= implode(',', $sqlValueArray);
                        $sqlValue .= ")";
                    }
                    else
                    {
                        if (empty($value))
                        {
                            $sqlValue = "''";
                        }
                        else
                        {
                            $sqlValue = '"' . $this->_connection->escape($value) . '"';
                        }
                    }
                    break;
                }
                
                if (is_array($value))
                {
                    array_push($sqlConditions, $sqlValue);
                }
                else
                {
                    if ($equalTo)
                    {
                        array_push($sqlConditions, "$name = $sqlValue");
                    }
                    else
                    {
                        array_push($sqlConditions, "$name <> $sqlValue");
                    }
                }
            }    
        }

        $sqlConditions = implode(' AND ', $sqlConditions);
        array_push($sqlLines, $sqlConditions);
    }

    private function _buildOrderSqlLines(Array $orderings, Array &$sqlLines)
    {
        $descOrderings = [];
        $ascOrderings = [];
        foreach ($orderings as $ordering)
        {
            foreach ($ordering as $direction => $column)
            {
                if ($direction == 'asc')
                {
                    array_push($ascOrderings, $column);
                }
                elseif ($direction == 'desc')
                {
                    array_push($descOrderings, $column);
                } 
            }
        }

        if (!empty($descOrderings))
        {
            $descColumns = implode(',', $descOrderings);
            array_push($sqlLines, "$descColumns DESC");
        }
        
        if (!empty($ascOrderings))
        {
            $ascColumns = implode(',', $ascOrderings);
            array_push($sqlLines, "$ascColumns ASC");
        }
    }
}
