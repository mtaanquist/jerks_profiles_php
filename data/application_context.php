<?php
namespace Data;

class ApplicationContext
{
    private $_connection;

    // add record sets here>
    public $profiles;
    public $games;
    public $sessions;
    public $gameOwnership;
    // add record sets here<

    public function __construct($connection)
    {
        $this->_connection = $connection;

        // add record sets here>
        $this->profiles = new \Data\RecordSet($connection, 'profile');
        $this->games = new \Data\RecordSet($connection, 'game');
        $this->sessions = new \Data\RecordSet($connection, 'update_token');
        $this->gameOwnership = new \Data\RecordSet($connection, 'profile_has_game');
        // add record sets here<   
    }
}
