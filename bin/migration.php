<?php
namespace Migration;

abstract class RecordMigration
{
    abstract protected function change();

    public function createTable($name, $columns = [])
    {

    }

    public function alterTable()
    {

    }

    public function deleteTable()
    {
        
    }
}