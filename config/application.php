<?php
namespace Config;

class Application
{
    private static $_environment;
    private static $_uri;
    private static $_method;

    public static function run($environment = 'production')
    {
        self::$_environment = $environment;
        
        self::$_uri = $_SERVER["REQUEST_URI"];
        self::$_method = $_SERVER["REQUEST_METHOD"];

        self::_handleRequest();
    }

    private static function _handleRequest()
    {
        self::_getRoute($controller, $action, $id);

        $controllerClassName = '\\App\\Controllers\\' . 
            str_replace('_', '', ucwords($controller, '_')) . 'Controller';
        if (class_exists($controllerClassName)) 
        {
            // get database context
            $context = new \Data\ApplicationContext(self::_getConnection());

            $controller = new $controllerClassName($context);
            if (!empty($action) && method_exists($controller, $action) 
                && is_callable(array($controller, $action))) 
            {
                self::_beforeLoadBody();
                $controller->$action($id);
                self::_afterLoadBody();
            } 
            else 
            {
                http_response_code(500);
                exit;
            }
        } 
        else 
        {
            throw new \Exception(
                'Unable to initialise class: ' . $controllerClassName);
        }
    }

    public static function getEnvironment()
    {
        return self::$_environment;
    }

    private static function _getConnection()
    {
        $connectionSettings = 
            yaml_parse_file('config/database.yml')
            [self::getEnvironment()];

        switch($connectionSettings["adapter"])
        {
        case 'sqlite3':
            $connection = new \Data\SqliteContext($connectionSetting);
            break;
        
        case 'mysql':
            $connection = new \Data\Mysql\MysqlConnection($connectionSettings);
            break;
        
        default:
            throw new \Exception(
                'Unsupported adapter: ' . $contextSettings["adapter"]);
        }

        $connection->open();

        return $connection;
    }    

    private static function _getRoute(&$controller, &$action, &$id)
    {
        include 'routes.php';
        $uriParts = explode('/', self::$_uri);

        $route = $routeMap[$uriParts[1]][$uriParts[2]];
        
        if (strtolower(self::$_method) !== $route['method']) 
        {
            exit(false);
        }
    
        $controller = $route['controller'];
        $action = $route['action'];
        $id = $uriParts[3];
    }

    private static function _beforeLoadBody(&$conn = null) 
    {  
        // we only want to load the header if it's a GET request
        if (self::$_method === "GET") 
        {
            include_once 'app/views/shared/layout/_header.php';        
        }
    }
    
    private static function _afterLoadBody(&$conn = null) 
    {
        // we only want to load the footer if it's a GET request
        if (self::$_method === "GET") 
        {
            include_once 'app/views/shared/layout/_footer.php';
        }
    } 
}
