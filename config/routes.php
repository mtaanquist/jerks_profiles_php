<?php
// mapping the routing to the various pages
$routeMap = [
    '' => [ // controller
        '' => [ // action
            'controller' => 'static_pages',
            'action' => 'home',
            'method' => 'get'
        ]
    ],
    'extension' => [
        '' => [
            'controller' => 'static_pages',
            'action' => 'extension',
            'method' => 'get'  
        ]
    ],
    'streams' => [
        '' => [
            'controller' => 'static_pages',
            'action' => 'streams',
            'method' => 'get'
        ]
    ],    
    'games' => [
        '' => [
            'controller' => 'game',
            'action' => 'index',
            'method' => 'get'
        ],
        'index' => [
            'controller' => 'game',
            'action' => 'index',
            'method' => 'get'
        ],
        'details' => [
            'controller' => 'game',
            'action' => 'details',
            'method' => 'get'
        ]
    ],
    'profiles' => [
        '' => [
            'controller' => 'profile',
            'action' => 'index',
            'method' => 'get'
        ],
        'details' => [
            'controller' => 'profile',
            'action' => 'details',
            'method' => 'get'
        ],        
        'create' => [
            'controller' => 'profile',
            'action' => 'create',
            'method' => 'get'
        ],
        'new' => [
            'controller' => 'profile',
            'action' => 'new',
            'method' => 'post'
        ],
        'edit' => [
            'controller' => 'profile',
            'action' => 'edit',
            'method' => 'get'
        ],
        'update' => [
            'controller' => 'profile',
            'action' => 'update',
            'method' => 'post'
        ],
        'signin' => [
            'controller' => 'profile',
            'action' => 'signin',
            'method' => 'post'
        ]
    ]    
];
