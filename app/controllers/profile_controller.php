<?php
namespace App\Controllers;

class ProfileController 
{
    private $_validateHelper;
    private $_context;

    function __construct($context)
    {
        $this->_context = $context;
        $this->$_validateHelper = new \App\Helpers\ProfileValidationHelper();
    }

    // GET: /profiles/
    function index()
    {
        include 'app/views/profile/index.php';
    }

    // GET: /profiles/details/$id
    function details($id)
    {
        if (empty($id)) 
        {
            http_response_code(404);
        }
        
        // get profile information
        $profile = $this->_context->profiles->findBy(['string:profileName' => $id]);
        $games = $this->_context->gameOwnership->select('game_id')->where(['int:profile_id' => $profile['id']])->all();
        
        $ownedGames = [];
        if (!empty($games))
            $ownedGames = $this->_context->games->where(['int:id' => array_column($games, 'game_id')])->all();

        include 'app/views/profile/details.php';
    }
    
    // GET: /profiles/create
    function create()
    {
        $games = $this->_context->games->all();

        include 'app/views/profile/create.php';
    }

    // POST: /profiles/new
    function new()
    {
        $formValidation = $this->$_validateHelper->validateFields($_POST);

        if ($formValidation === true && strlen($_POST["profileName"]) > 0) {
            $profileName = $_POST["profileName"];
    
            // check if user entered a passphrase first, if not, create one
            $profilePassphrase = (empty($_POST["profilePassphrase"])) 
                ? $this->_generateRandomString() 
                : $_POST["profilePassphrase"];
    
            $createProfileSql = "INSERT INTO profile (profileName, profilePassphrase) VALUES (\"$profileName\", \"$profilePassphrase\")";
            $result = $this->_context->profiles->sql($createProfileSql);
    
            if (!$result)
            {
                $this->_generateResultPage("error", $formValidation);
                exit;
            }

            $profile = $this->_context->profiles->last();
            $this->_updateProfile($profile["id"], $_POST);
            $this->_generateResultPage("success", "", $profileName, $profilePassphrase);
        } 
        else 
        {
            $this->_generateResultPage("error", $formValidation);
            exit;
        }
    }

    // GET: /profiles/edit
    function edit($id)
    {
        if (empty($id)) 
        {
            http_response_code(404);
        }

        $session = $this->_context->sessions->findBy(['string:token_content' => $id]);
        $profile = $this->_context->profiles->find($session['profile_id']);
        
        // get their games owned
        $gamesOwned = array_column($this->_context->gameOwnership->where(['int:profile_id' => $profile["id"]])->all(), 'game_id');
        $games = $this->_context->games->all();

        include 'app/views/profile/edit.php';
    }

    // POST: /profiles/update
    function update($id)
    {
        $formValidation = $this->$_validateHelper->validateFields($_POST, $expectations);
        if ($formValidation === true && !empty($id)) 
        {
            $session = $this->_context->sessions->findBy(['string:token_content' => $id]);
            $profile = $this->_context->profiles->find($session['profile_id']);
    
            $this->_updateProfile($profile['id'], $_POST);
    
            // remove token(s) for the profile when done
            $sql = "DELETE FROM update_token WHERE profile_id = " . $profile['id'];
            $this->_context->sessions->sql($sql);

            // send user back to their profile page
            header("Location: /profiles/details/" . $profile['profileName']);
        } 
        else 
        {
            $this->_generateResultPage("error", $formValidation);
        }
    }

    // POST: /profiles/signin
    function signin()
    {
        if (!empty($_POST['profileName'])) 
        {
            $formValidation = $this->$_validationHelper->validateFields($_POST);
            if ($formValidation === true) 
            {
                $profile = $this->_context->profiles
                    ->where(['string:profileName' => $_POST['profileName'], 'string:profilePassphrase' => $_POST["profilePassphrase"]])
                    ->take();

                $profileId = $profile['id'];
                // to avoid exposing profileId, we use a token instead
                // let's generate one
                $tokenContent = $this->_generateRandomString(16);
                $sql = "INSERT INTO update_token (token_content, profile_id) VALUES (\"$tokenContent\", $profileId)";
                
                if ($this->_context->profiles->sql($sql))
                    header("Location: /profiles/edit/" . $tokenContent);        
            }
        }
    
        $this->_generateResultPage(
            "error", 
            "Authentication failed. Please check the data you entered and try again."
        );
    }

    // Private
    private function _generateRandomString($length = 10) 
    {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $randomString;
    }

    private function _updateProfile(int $profileId, Array $formData)
    {
        $sql = "UPDATE profile SET ";
        $values = [];

        foreach ($formData as $key => $value) {
            // skip some fields
            $skipFields = ['games', 'profileName', 'profilePassphrase'];
            if (in_array($key, $skipFields)) {
                continue;
            }

            $sql .= $this->_context->profiles->escape($key) . " = \"". $this->_context->profiles->escape($value) ."\", ";
            array_push($values, $value);
        }

        array_push($values, $profileId);
        $sql .= "profileLastUpdated = NOW() WHERE id = $profileId";
        
        array_unshift($values, $sql);
        $this->_context->profiles->sql($sql);
        
        // check user selected any games at all, or we'll be looking at an error
        if (isset($formData["games"])) {
            // first, remove old entries
            $sql = "DELETE FROM profile_has_game WHERE profile_id = $profileId";
            $this->_context->profiles->sql($sql);

            foreach ($formData["games"] as $gameId => $gameOwned) {
                // then, insert new data
                $sql = "INSERT INTO profile_has_game (profile_id, game_id) VALUES ($profileId, $gameId)";
                $this->_context->profiles->sql($sql);
            }
        }
    }    

    private function _generateResultPage($type, $message = "", $profileName = "", $passphrase = "") 
    {
        switch ($type) {
        case "error":
            echo '
            <div class="container">
                <div class="col-md-12">
                    <h3 class="page-lead">Oh no! Something went wrong.</h3>
                    <p><strong>Error message:</strong> '. $message .'</p>
                    <hr />
                    <p>Go back to the <a href="#" onclick="history.go(-1)">previous page</a>.</p>
                </div>
            </div>
            ';
            break;
        case "success":
            echo '
            <div class="container">
                <div class="col-md-12">
                    <h3 class="page-lead">Success!</h3>
                    <p class="lead">Thank you, <strong>'. $profileName .'</strong>! Your profile was updated successfully.</p>
                    <p>Should you need to update your profile again, your passphrase is: <strong>' . $passphrase . '</strong></p>
                    <hr />
                    <p>Go back to <a href="/">the frontpage</a>.</p>
                </div>
            </div>
            ';
            break;
        }
    }
}
?>
