<?php
namespace App\Controllers;

class GameController
{
    private $_context;
    
    function __construct($context)
    {
        $this->_context = $context;
    }

    // GET: /games[/index]
    function index() 
    {
        $pcGames = $this->_context->games
                                ->where(['string:gameSystem' => ['Origin', 'Steam', 'Blizzard', 'PC']])
                                ->order(['asc' => 'gameName'])
                                ->all();

        $psGames = $this->_context->games
                                ->where(['string:gameSystem' => ['PS3', 'PS4', 'PSN']])
                                ->order(['asc' => 'gameName'])
                                ->all();

        $xboxGames = $this->_context->games
                                ->where(['string:gameSystem' => ['X360', 'X1', 'XBOX']])
                                ->order(['asc' => 'gameName'])
                                ->all();


        $nintendoGames = $this->_context->games
                                ->where(['string:gameSystem' => ['3DS', 'WiiU', 'Nintendo', 'Switch']])
                                ->order(['asc' => 'gameName'])
                                ->all();

        $mobileGames = $this->_context->games
                                ->where(['string:gameSystem' => ['iOS', 'Android', 'WinMobile', 'Mobile']])
                                ->order(['asc' => 'gameName'])
                                ->all();        

        include 'app/views/game/index.php';
    }

    // GET: /games/details/$id
    function details($id) 
    {
        if (empty($id)) 
        {
            http_response_code(404);
        }
        
        $game = $this->_context->games->find($id);

        switch ($id) {
        case 23:      
            $profiles = $this->_context->profiles->not(['string:lolName' => null])->all();
            break;
        
        case 24:      
            $profiles = $this->_context->profiles->not(['string:poeName' => null])->all();
            break;
        
        case 28:       
            $profiles = $this->_context->profiles->not(['string:ffrkFriendId' => null])->all();
            break;
        
        case 29:      
            $profiles = $this->_context->profiles->not(['string:padName' => null])->all();
            break;
        
        default:
            $gameOwners = $this->_context->gameOwnership->where(['int:game_id' => $id])->all();
            $profiles = $this->_context->profiles->find(array_column($gameOwners, 'profile_id'));

            break;
        }

        include 'app/views/game/details.php';
    }    
}
