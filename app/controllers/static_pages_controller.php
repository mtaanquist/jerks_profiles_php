<?php
namespace App\Controllers;

class StaticPagesController
{
    private $_context;
    
    function __construct($context)
    {
        $this->_context = $context;
    }

    function home() 
    {
        $profiles = $this->_context->profiles->all();

        include 'app/views/static_pages/home.php';
    }

    function streams()
    {
        include 'app/views/static_pages/streams.php';
    }        

    function extension()
    {
        include 'app/views/static_pages/extension.php';
    }
}

?>
