<?php
    
$action = $_GET['Action'];
$item = $_GET['Id'];

if (!empty($action)) {
    switch (strtolower($action)) {
    case "profiles":
        $sql = "SELECT profileName, steamName, steamUrl, originName, bnetName, 
                psnName, xboxName, nintendoWiiUName, nintendoFriendCode, iosName, 
                facebookUrl, googlePlusUrl, linkedInUrl, twitterName, twitchName 
                FROM profile";
        $profiles = [];

        if (!empty($item)) {
            $sql .= " WHERE profileName = %s LIMIT 1";
            $profiles = $conn->query($sql, $item);
        }
        else
        {
            $profiles = $conn->query($sql);
        }

        // Return the result as JSON.
        if (!empty($profiles)) echo json_encode($profiles);

        break;
    case "streamstatus":
        $sql = "SELECT streamId FROM live_stream 
                JOIN profile ON profile.profileId = live_stream.profileId";
        
        $streams = [];
        if (!empty($item))
        {
            $sql .= " WHERE profile.profileName = %s";
            $streams = $conn->query($sql, $item);
        }
        else
        {
            $streams = $conn->query($sql);
        }

        // Return the result as JSON.
        if (!empty($streams)) echo json_encode($streams);
        break;
    }
}