<div class="container">

    <div class="row">
        <div class="col-md-10">

            <h3 class="page-header">Profiles</h3>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th class="profile">Name</th>
                            <th class="steam"><i class="fa fa-steam-square"></i> Steam</th>
                            <th class="origin hidden">Origin</th>
                            <th class="blizzard">Battle.net</th>
                            <th class="psn">PSN</th>
                            <th class="xbox hidden">XBOX</th>
                            <th class="nin-switch hidden">WiiU</th>
                            <th class="wiiu hidden">WiiU</th>
                            <th class="3ds hidden">3DS</th>
                            <th class="ios hidden">iOS</th>
                            <th class="ello hidden">Ello</th>
                            <th class="facebook hidden"><i class="fa fa-facebook-square"></i>Facebook</th>
                            <th class="googleplus hidden"><i class="fa fa-google-plus-square"></i> Google+</th>
                            <th class="linkedin hidden"><i class="fa fa-linkedin-square"></i> LinkedIn</th>
                            <th class="twitter"><i class="fa fa-twitter-square"></i> Twitter</th>
                            <th class="twitch hidden">Twitch</th>
                            <th class="youtube hidden">YouTube</th>
                        </tr>
                    </thead>
                    <tbody>
<?php
foreach ($profiles as $profile) {
?>
                        <tr>
                            <td class="profile"><a href="/profiles/details/<?= $profile['profileName'] ?>/"><?= $profile['profileName'] ?></a></td>
                            <td class="steam">
                                <?php if (!empty($profile['steamUrl'])) { ?>
                                    <a href="<?= $profile['steamUrl'] ?>" target="_blank">
                                    <?php if (!empty($profile['steamName'])) { ?>
                                        <?= $profile['steamName'] ?>
                                    <?php } ?>
                                    </a>
                                <?php } else { ?>
                                    <?php if (!empty($profile['steamName'])) { ?>
                                        <?= $profile['steamName'] ?>
                                    <?php } ?>
                                <?php } ?>
                            </td>
                            <td class="origin hidden"><?php if (!empty($profile['originName'])) { ?><?= $profile['originName'] ?><?php } ?></td>
                            <td class="blizzard"><?php if (!empty($profile['bnetName'])) { ?><?= $profile['bnetName'] ?><?php } ?></td>
                            <td class="psn"><?php if (!empty($profile['psnName'])) { ?><?= $profile['psnName'] ?><?php } ?></td>
                            <td class="xbox hidden"><?php if (!empty($profile['xboxName'])) { ?><?= $profile['xboxName'] ?><?php } ?></td>
                            <td class="nin-switch hidden"><?php if (!empty($profile['nintendoSwitchFriendCode'])) { ?><?= $profile['nintendoSwitchFriendCode'] ?><?php } ?></td>
                            <td class="wiiu hidden"><?php if (!empty($profile['nintendoWiiUName'])) { ?><?= $profile['nintendoWiiUName'] ?><?php } ?></td>
                            <td class="3ds hidden"><?php if (!empty($profile['nintendoFriendCode'])) { ?><?= $profile['nintendoFriendCode'] ?><?php } ?></td>
                            <td class="ios hidden"><?php if (!empty($profile['iosName'])) { ?><?= $profile['iosName'] ?><?php } ?></td>
                            <td class="ello hidden"><?php if (!empty($profile['elloName'])) { ?><a href="https://ello.co/<?= $profile['elloName'] ?>"><?= $profile['elloName'] ?></a><?php } ?></td>
                            <td class="facebook hidden"><?php if (!empty($profile['facebookUrl'])) { ?><a href="<?= $profile['facebookUrl'] ?>">Profile</a><?php } ?></td>
                            <td class="googleplus hidden"><?php if (!empty($profile['googlePlusUrl'])) { ?><a href="<?= $profile['googlePlusUrl'] ?>">Profile</a><?php } ?></td>
                            <td class="linkedin hidden"><?php if (!empty($profile['linkedInUrl'])) { ?><a href="<?= $profile['linkedInUrl'] ?>">Profile</a><?php } ?></td>
                            <td class="twitter"><?php if (!empty($profile['twitterName'])) { ?><a href="https://twitter.com/<?= $profile['twitterName'] ?>">@<?= $profile['twitterName'] ?></a><?php } ?></td>
                            <td class="twitch hidden"><?php if (!empty($profile['twitchName'])) { ?><a href="http://www.twitch.tv/<?= $profile['twitchName'] ?>"><?= $profile['twitchName'] ?></a><?php } ?></td>
                            <td class="youtube hidden"><?php if (!empty($profile['youtubeName'])) { ?><a href="https://www.youtube.com/<?= $profile['youtubeName'] ?>"><?= $profile['youtubeName'] ?></a><?php } ?></td>
                        </tr>
<?php
    }
?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-2">
            <div class="fixed">
                <h3 class="page-header">Filters</h3>
                <form role="form">
                    <h5>Platforms</h5>
                    <div class="checkbox">
                        <label>
                            <input id="cbToggleSteam" type="checkbox" checked><i class="fa fa-steam-square"></i> Steam 
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input id="cbToggleOrigin" type="checkbox"> Origin 
                        </label>
                    </div>
                    <h5>Services</h5>
                    <div class="checkbox">
                        <label>
                            <input id="cbToggleBlizzard" type="checkbox" checked> Battle.net 
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input id="cbTogglePsn" type="checkbox" checked> PlayStation Network 
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input id="cbToggleXbox" type="checkbox"> XBOX Live 
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input id="cbToggleNinSwitch" type="checkbox"> Nintendo Switch
                        </label>
                    </div>					
                    <div class="checkbox">
                        <label>
                            <input id="cbToggleWiiU" type="checkbox"> WiiU
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input id="cbToggle3ds" type="checkbox"> 3DS
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input id="cbToggleIos" type="checkbox"> iOS
                        </label>
                    </div>
                    <h5>Social Networks</h5>
                    <div class="checkbox">
                        <label>
                            <input id="cbToggleEllo" type="checkbox">Ello
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input id="cbToggleFacebook" type="checkbox"><i class="fa fa-facebook-square"></i> Facebook
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input id="cbToggleGooglePlus" type="checkbox"><i class="fa fa-google-plus-square"></i> Google+
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input id="cbToggleLinkedIn" type="checkbox"><i class="fa fa-linkedin-square"></i> LinkedIn
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input id="cbToggleTwitter" type="checkbox" checked><i class="fa fa-twitter-square"></i> Twitter
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input id="cbToggleTwitch" type="checkbox"><i class="fa fa-twitch"></i> Twitch
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input id="cbToggleYouTube" type="checkbox"><i class="fa fa-youtube"></i> YouTube
                        </label>
                    </div>					
                </form>
            </div>
        </div>
    </div>

</div>

<script>
    $('#cbToggleSteam').click(function () {
        $('.steam').toggleClass('hidden');
    });
    $('#cbToggleOrigin').click(function () {
        $('.origin').toggleClass('hidden');
    });
    $('#cbToggleBlizzard').click(function () {
        $('.blizzard').toggleClass('hidden');
    });
    $('#cbTogglePsn').click(function () {
        $('.psn').toggleClass('hidden');
    });
    $('#cbToggleXbox').click(function () {
        $('.xbox').toggleClass('hidden');
    });
    $('#cbToggleWiiU').click(function () {
        $('.wiiu').toggleClass('hidden');
    });
    $('#cbToggleNinSwitch').click(function () {
        $('.nin-switch').toggleClass('hidden');
    });
    $('#cbToggle3ds').click(function () {
        $('.3ds').toggleClass('hidden');
    });
    $('#cbToggleIos').click(function () {
        $('.ios').toggleClass('hidden');
    });
    $('#cbToggleEllo').click(function () {
        $('.ello').toggleClass('hidden');
    });
    $('#cbToggleFacebook').click(function () {
        $('.facebook').toggleClass('hidden');
    });
    $('#cbToggleGooglePlus').click(function () {
        $('.googleplus').toggleClass('hidden');
    });
    $('#cbToggleLinkedIn').click(function () {
        $('.linkedin').toggleClass('hidden');
    });
    $('#cbToggleTwitter').click(function () {
        $('.twitter').toggleClass('hidden');
    });
    $('#cbToggleTwitch').click(function () {
        $('.twitch').toggleClass('hidden');
    });
    $('#cbToggleYouTube').click(function () {
        $('.youtube').toggleClass('hidden');
    });
</script>
