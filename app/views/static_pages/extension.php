<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2 class="page-header">Browser Extension <small>Available for Chrome and Firefox</small></h2>
            <h4>The AJ Profiles Integration extension allows you to see information of forum posters taken directly from this site, making it less necessary to visit this site to find profile names.</h4>
		</div>
	</div>
    <div class="row">
        <div class="col-md-6">
            <h3>Chrome <small>1.5.1</small></h3>
            <p>You can download the Chrome extension from the <a href="https://chrome.google.com/webstore/detail/aj-profiles-integration/jcnbmpdmmfpekhonppopcfhmmpjoiadg?hl=en-US">Chrome Web Store</a>.</p>
            <p><strong>Installation notes:</strong> After installing the add-on, you'll be shown an Options page where you can select which profile fields you'd like to see. If none are marked, this add-on will not function.</p>
        </div>
        <div class="col-md-6">
            <h3>Firefox <small>1.5.1</small></h3>
            <p>You can download the Firefox extension from here: <a href="https://karantor.com/files/ajp-integration.xpi">Download Add-on</a>.</p>
            <p><strong>Installation notes:</strong> After installing the add-on, please make sure to visit the Options page from the Add-ons Manager to set the preferences of which profile fields you'd like to see.</p>
        </div>      
    </div>
    <div class="row">
        <div class="col-md-12">
            <h2>Features</h2>
            <p>The extension currently provides the following features for integration:</p>
            <ul>
                <li>Include profile fields from a user's profile in the author information region on the left side of posts in both threads and private messages.</li>
                <li>If the Twitch profile field is included to be shown, it also shows if the poster is currently online.</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h2>Change Log</h2>
            <p>Find the latest release notes below.</p>

            <h4>Version 1.5.1 <small>2014/11/09</small></h4>
            <ul>
                <li>Bug fixes and performance improvements.</li>
                <li>Additional prevention for XSS attacks.</li>
                <li>Clicking the "AJ Profiles" text now brings you to the specific user's profile on the Profiles site.</li>
            </ul>

            <h4>Version 1.5.0 <small>2014/11/04</small></h4>
            <ul>
                <li>New version for Firefox launch.</li>
            </ul>
            
            <h4>Version 1.4.6 <small>2014/11/03</small></h4>
            <ul>
                <li>Removed debug code.</li>
            </ul>

            <h4>Version 1.4.3 <small>2014/11/02</small></h4>
            <ul>
                <li>Fixed an issue where the profile cache wouldn't properly expire.</li>
            </ul>

            <h4>Version 1.4.2 <small>2014/11/02</small></h4>
            <ul>
                <li>Performance improvements and bug fixes.</li>
                <li>An update to the cache structure may cause issues. If you notice anything odd, please use the "Expire Cache" button on the Options panel.</li>
            </ul>

            <h4>Version 1.4.0 <small>2014/11/01</small></h4>
            <ul>
                <li>Fields that can link directly to the specific profile (e.g. Steam's community page) are now clickable.</li>
                <li>Indicator next to Twitch channel name if channel is live (updated every five minutes).</li>
                <li>Option to expire profile cache from the Options page.</li>
            </ul>
        </div>
    </div>
</div>
