<?php
$liveChannels = $conn->query(
    "SELECT * FROM live_stream 
    JOIN profile ON live_stream.profileId = profile.profileId"
);
?>

<div class="container">
    <div class="row">
        <div class="col-md-10">
            <h3 class="page-header">Live Streamers <small>(List updated every five minutes)</small></h3>
<?php
if (is_array($liveChannels) && count($liveChannels) > 0) {
?>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Channel</th>
                            <th>Stream Title</th>
                            <th>Currently Playing</th>
                            <th>Viewers</th>
                        </tr>
                    </thead>
                    <tbody>
<?php
foreach ($liveChannels as $liveChannel) {
?>
                        <tr>
                            <td><?= $liveChannel['profileName'] ?></td>
                            <td><a href="http://www.twitch.tv/<?= strtolower($liveChannel['streamChannelName']) ?>/" target="_blank"><?= $liveChannel['streamChannelName'] ?></a></td>
                            <td><?= $liveChannel['streamTitle'] ?></td>
                            <td><?= $liveChannel['streamCurrentGame'] ?></td>
                            <td><?= $liveChannel['streamViewerCount'] ?></td>
                        </tr>
<?php
}
?>
                    </tbody>
                </table>
            </div>
<?php
} else {
?>
            <p>No streams are currently online. Check back later!</p>
<?php
}
?>
        </div>
        <div class="col-md-2">
            <h3 class="page-header">Twitch.tv</h3>
            <p>Twitch.tv is an online platform for streaming your gameplay to others who might wish to watch you play.</p>
            <p>To appear on this list when you go live, simply ensure that you've added your Twitch.tv channel name to your profile.</p>
        </div>
    </div>
</div>
