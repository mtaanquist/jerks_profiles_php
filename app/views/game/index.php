<div class="container">
    <div class="row">
        <div class="col-md-10">
            <h3 class="page-header">PC games</h3>
            <p>PC games comprise the following platforms and services: Steam, Origin, Battle.net, as well as some of the individual games.</p>
            <div class="row">
<?php
foreach ($pcGames as $pcGame) {
?>
                <div class="col-md-3">
                    <p>
                        <a href="/games/details/<?= $pcGame['id'] ?>" class="btn btn-link btn-block"><?= $pcGame['gameName'] ?></a>
                    </p>
                </div>
<?php
}
?>
            </div>

            <h3 class="page-header">PlayStation games</h3>
            <p>PlayStation games comprise the following platforms and services: PlayStation Network, PS3, PS4</p>
            <div class="row">
<?php
foreach ($psGames as $psGame) {
?>
                <div class="col-md-3">
                    <p>
                        <a href="/games/details/<?= $psGame['id'] ?>" class="btn btn-link btn-block"><?= $psGame['gameName'] ?> (<?= $psGame['gameSystem'] ?>)</a>
                    </p>
                </div>
<?php
}
?>
            </div>

            <h3 class="page-header">XBOX games</h3>
            <p>XBOX games comprise the following platforms and services: XBOX Live, XBOX 360, XBOX One</p>
            <div class="row">
<?php
foreach ($xboxGames as $xboxGame) {
?>
                <div class="col-md-3">
                    <p>
                        <a href="/games/details/<?= $xboxGame['id'] ?>" class="btn btn-link btn-block"><?= $xboxGame['gameName'] ?> (<?= $xboxGame['gameSystem'] ?>)</a>
                    </p>
                </div>
<?php
}
?>
            </div>

            <h3 class="page-header">Nintendo games</h3>
            <p>Nintendo games comprise the following platforms and services: Nintendo, Wii, WiiU, and 3DS</p>
            <div class="row">
<?php
foreach ($nintendoGames as $nintendoGame) {
?>
                <div class="col-md-3">
                    <p>
                        <a href="/games/details/<?= $nintendoGame['id'] ?>" class="btn btn-link btn-block"><?= $nintendoGame['gameName'] ?> (<?= $nintendoGame['gameSystem'] ?>)</a>
                    </p>
                </div>
<?php
}
?>
            </div>

            <h3 class="page-header">Mobile games</h3>
            <p>Mobile games comprise the following platforms and services: iOS, Android, and Windows Mobile</p>
            <div class="row">
<?php
foreach ($mobileGames as $mobileGame) {
?>
                <div class="col-md-3">
                    <p>
                        <a href="/games/details/<?= $mobileGame['id'] ?>" class="btn btn-link btn-block"><?= $mobileGame['gameName'] ?></a>
                    </p>
                </div>
<?php
}
?>
            </div>			
        </div>
        <div class="col-md-2">
            <h3 class="page-header">Suggestions?</h3>
            <p>If you have a game you'd like to see added to the list, please drop me a private message on the forum.</p>
        </div>

        <!-- <div class="col-md-2">
            <h3 class="page-header">Add new game (NYI)</h3>
            <form id="formAddGame" method="post" action="#" role="form">
                <div class="form-group">
                    <label>Game name</label>
                    <div id="igdb-lookup">
                        <input type="text" class="form-control typeahead" name="gameName" placeholder="Enter game name" autocomplete="off">
                    </div>
                </div>
            </form>
        </div> -->
    </div>
</div>

<script src="/assets/js/page.games.js"></script>
