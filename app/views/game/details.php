<div class="container">
    <div class="col-md-12">
        <h3 class="page-header">Players for <?= $game["gameName"] ?> (<?= $game["gameSystem"] ?>)</h3>
<?php
if (is_array($profiles) && count($profiles) > 0) {
?>
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Profile Name</th>
                        <th <?php if ($game['gameSystem'] != "Steam") echo 'class="hidden"'; ?>>Steam</th>
                        <th <?php if ($game['gameSystem'] != "Origin") echo 'class="hidden"'; ?>>Origin</th>
                        <th <?php if ($game['gameSystem'] != "Blizzard") echo 'class="hidden"'; ?>>BattleTag</th>
                        <th <?php if ($game['gameSystem'] != "Blizzard") echo 'class="hidden"'; ?>>Region</th>
                        <th <?php if ($game['gameSystem'] != "PS4" && $game['gameSystem'] != "PS3") echo 'class="hidden"'; ?>>PSN</th>
                        <th <?php if ($game['gameSystem'] != "X1" && $game['gameSystem'] != "X360") echo 'class="hidden"'; ?>>XBOX</th>
                        <th <?php if ($game['gameSystem'] != "WiiU") echo 'class="hidden"'; ?>>WiiU</th>
                        <th <?php if ($game['gameSystem'] != "3DS") echo 'class="hidden"'; ?>>3DS Friend Code</th>
                        <th <?php if ($game["gameName"] !== "League of Legends") echo 'class="hidden"'; ?>>Summoner Name</th>
                        <th <?php if ($game["gameName"] !== "League of Legends") echo 'class="hidden"'; ?>>Region</th>
                        <th <?php if ($game["gameName"] !== "Path of Exile") echo 'class="hidden"'; ?>>Character Name</th>
                        <th <?php if ($game["gameName"] !== "FINAL FANTASY Record Keeper") echo 'class="hidden"'; ?>>Friend ID</th>
                        <th <?php if ($game["gameName"] !== "Puzzle & Dragons") echo 'class="hidden"'; ?>>Profile Code</th>
                    </tr>
                </thead>
                <tbody>
<?php
foreach ($profiles as $player) {
?>
                    <tr>
                        <td><a href="/profiles/details/<?= $player["profileName"]?>/"><?= $player['profileName'] ?></a></td>
                        <td <?php if ($game['gameSystem'] != "Steam") echo 'class="hidden"'; ?>><?= $player['steamName'] ?></td>
                        <td <?php if ($game['gameSystem'] != "Origin") echo 'class="hidden"'; ?>><?= $player['originName'] ?></td>
                        <td <?php if ($game['gameSystem'] != "Blizzard") echo 'class="hidden"'; ?>><?= $player['bnetName'] ?></td>
                        <td <?php if ($game['gameSystem'] != "Blizzard") echo 'class="hidden"'; ?>><?= $player['bnetRegion'] ?></td>
                        <td <?php if ($game['gameSystem'] != "PS4" && $game['gameSystem'] != "PS3") echo 'class="hidden"'; ?>><?= $player['psnName'] ?></td>
                        <td <?php if ($game['gameSystem'] != "X1" && $game['gameSystem'] != "X360") echo 'class="hidden"'; ?>><?= $player['xboxName'] ?></td>
                        <td <?php if ($game['gameSystem'] != "WiiU") echo 'class="hidden"'; ?>><?= $player['nintendoWiiUName'] ?></td>
                        <td <?php if ($game['gameSystem'] != "3DS") echo 'class="hidden"'; ?>><?= $player['nintendoFriendCode'] ?></td>
                        <td <?php if ($game["gameName"] !== "League of Legends") echo 'class="hidden"'; ?>><?= $player["lolName"] ?></td>
                        <td <?php if ($game["gameName"] !== "League of Legends") echo 'class="hidden"'; ?>><?= $player["lolRegion"] ?></td>
                        <td <?php if ($game["gameName"] !== "Path of Exile") echo 'class="hidden"'; ?>><?= $player["poeName"] ?></td>
                        <td <?php if ($game["gameName"] !== "FINAL FANTASY Record Keeper") echo 'class="hidden"'; ?>><?= $player["ffrkFriendId"] ?></td>
                        <td <?php if ($game["gameName"] !== "Puzzle & Dragons") echo 'class="hidden"'; ?>><?= $player["padName"] ?></td>
                    </tr>	
<?php
}
?>
                </tbody>
            </table>
        </div>
<?php
} else {
?>
        <p>No profiles found that have selected that they play <?= $game["gameName"] ?>. Sorry!</p>
<?php
}
?>
    <p>Go back the to <a href="/games/">game list</a>.</p>
    </div>
</div>