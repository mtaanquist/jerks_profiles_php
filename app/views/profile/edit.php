	<div class="container">
	  <div class="row">
		<div class="col-md-9" role="main">
		  <h3 id="overview" class="page-header">Overview</h3>

		  <h4 id="overview-forminfo">Mostly optional</h4>
		  <p>This form is fairly lengthy, which is why the navigation menu to the right exists. There are very few fields that are required, and as such can be left blank if you'd like to.</p>

		  <h4 id="overview-datausage">Data usage</h4>
		  <p>Your data is only used for display in the table on this page/application. It's not being sold or traded, nor is tracking information employed. Normal Internet etiquette should always be considered, though, so don't share anything you don't want people to find out.</p>

		  <h4 id="overview-contact">Contact</h4>
		  <p>Should you have any questions regarding this form (or the page), please don't hesitate in shooting me an <a href="mailto:jerks@karantor.com">e-mail</a>. You should receive a reply within 24 hours.</p>

		  <form id="formUpdateProfile" method="post" action="/profiles/update/<?= $id ?>" role="form">
			<h3 id="profile" class="page-header">Your Profile</h3>
			<p class="lead"></p>

			<div class="form-group">
			  <label>Forum username (required)</label>
			  <input type="text" class="form-control" name="profileName" placeholder="Enter forum username" value="<?= $profile['profileName'] ?>" disabled="disabled">
			  <p class="help-block">You cannot change this after creation.</p>
			</div>

			<div class="form-group">
			  <label>Profile passphrase</label>
			  <input type="text" class="form-control" name="profilePassphrase" placeholder="Provide a passphrase" value="<?= $profile['profilePassphrase'] ?>" disabled="disabled">
			  <p class="help-block">You cannot change this after creation.</p>
			</div>


			<h3 id="platforms" class="page-header">Platforms</h3>
			<p class="lead"></p>

			<h4 id="platforms-steam">Steam</h4>
			<p>Both your Steam name and Steam profile page link address can be changed by yourself, so make sure that you check the information in your Steam client before saving your profile.</p>
			
			<div class="form-group">
			  <label>Steam name</label>
			  <input type="text" class="form-control" name="steamName" value="<?= $profile['steamName'] ?>" placeholder="Enter Steam name">
			  <p class="help-block">Your Steam name is the name that is shown in games, or when you're talking with people from the Friends List.</p>
			</div>

			<div class="form-group">
			  <label>Steam profile page</label>
			  <input type="text" class="form-control" name="steamUrl" value="<?= $profile['steamUrl'] ?>" placeholder="Enter Steam profile page address">
			  <p class="help-block">You can find your profile page address by hovering over your name in the top navigation bar within the Steam client, then selecting "Profile." Once the page has loaded, right-click in a blank space and select "Copy Page URL."</p>
			</div>

			<div class="form-group">
			  <label>Valve multi-player games played</label>
<?php
  foreach ($games as $game)
  {
	if ($game["gameSystem"] !== 'Steam') continue;
?>
			  <div class="checkbox">
				<label>
				  <input type="checkbox" name="games[<?= $game['id'] ?>]" <?php if (in_array($game['id'], $gamesOwned)) echo 'checked="checked"'; ?>> <?= $game['gameName'] ?>
				</label>
			  </div>
<?php
  }
?>
			</div>

			<p>Please <a href="#overview-contact">let me know</a> if there's demand for further Valve multi-player games to be added here.</p>


			<h4 id="platforms-origin">Origin</h4>
			<p></p>

			<div class="form-group">
			  <label>Origin ID</label>
			  <input type="text" class="form-control" name="originName" value="<?= $profile['originName'] ?>" placeholder="Enter Origin ID">
			  <p class="help-block">Your Origin ID is the name shown in the upper-right corner of the Origin client.</p>
			</div>

			<div class="form-group">
			  <label>EA multi-player games played</label>
<?php
  foreach ($games as $game)
  {
	if ($game["gameSystem"] !== 'Origin') continue;
?>
			  <div class="checkbox">
				<label>
				  <input type="checkbox" name="games[<?= $game['id'] ?>]"> <?= $game['gameName'] ?>
				</label>
			  </div>
<?php
  }
?>
			</div>

			<p>Please <a href="#overview-contact">let me know</a> if there's demand for further EA multi-player games to be added here.</p>

			<h3 id="services" class="page-header">Services</h3>
			<p class="lead"></p>

			<h4 id="services-bnet">Battle.net</h4>
			<p></p>

			<div class="form-group">
			  <label>BattleTag</label>
			  <input type="text" class="form-control" name="bnetName" value="<?= $profile['bnetName'] ?>" placeholder="Enter BattleTag">
			  <p class="help-block">Include both the name and identifier, e.g.: "SomeName#1234"</p>
			</div>

			<div class="form-group">
			  <label>Primary region</label>
			  <select class="form-control" name="bnetRegion">
				<option <?php if ($profile['bnetRegion'] === "Americas") echo 'selected="selected"'; ?>>Americas</option>
				<option <?php if ($profile['bnetRegion'] === "Europe") echo 'selected="selected"'; ?>>Europe</option>
				<option <?php if ($profile['bnetRegion'] === "Asia") echo 'selected="selected"'; ?>>Asia</option>
			  </select>
			  <p class="help-block">Battle.net allows you to log into most regions, but which would you consider your primary region?</p>
			</div>

			<div class="form-group">
			  <label>Battle.net compatible multi-player games played</label>
<?php
  foreach ($games as $game)
  {
	if ($game["gameSystem"] !== 'Blizzard') continue;
?>
			  <div class="checkbox">
				<label>
				  <input type="checkbox" name="games[<?= $game['id'] ?>]" <?php if (in_array($game['id'], $gamesOwned)) echo 'checked="checked"'; ?>> <?= $game['gameName'] ?>
				</label>
			  </div>
<?php
  }
?>
			</div>

			<p>Please <a href="#overview-contact">let me know</a> if there's demand for further Battle.net/Blizzard multi-player games to be added here.</p>

			<h4 id="services-psn">PlayStation Network</h4>
			<p></p>

			<div class="form-group">
			  <label>PSN ID</label>
			  <input type="text" class="form-control" name="psnName" value="<?= $profile['psnName'] ?>" placeholder="Enter PSN ID">
			</div>

			<div class="form-group">
			  <label>Primary region</label>
			  <select class="form-control" name="psnRegion">
				<option <?php if ($profile['psnRegion'] === "Americas") echo 'selected="selected"'; ?>>Americas</option>
				<option <?php if ($profile['psnRegion'] === "Europe") echo 'selected="selected"'; ?>>Europe</option>
				<option <?php if ($profile['psnRegion'] === "Asia") echo 'selected="selected"'; ?>>Asia</option>
			  </select>
			</div>

			<div class="form-group">
			  <label>PlayStation Network compatible multi-player games played</label>
<?php
foreach ($games as $game)
{
	if ($game["gameSystem"] !== 'PS3' || $game["gameSystem"] !== 'PS4') continue;
?>
			  <div class="checkbox">
				<label>
				  <input type="checkbox" name="games[<?= $game['id'] ?>]" <?php if (in_array($game['id'], $gamesOwned)) echo 'checked="checked"'; ?>> <?= $game['gameName'] ?> (<?= $game['gameSystem'] ?>)
				</label>
			  </div>
<?php
}
?>
			</div>
			<p>Please <a href="#overview-contact">let me know</a> if there's demand for further PlayStation Network multi-player games to be added here.</p>         
			
			<h4 id="services-xbox">XBOX Live</h4>
			<p></p>

			<div class="form-group">
			  <label>XBOX Live Name</label>
			  <input type="text" class="form-control" name="xboxName" value="<?= $profile['xboxName'] ?>" placeholder="Enter XBOX Live name">
			</div>

			<div class="form-group">
			  <label>Primary region</label>
			  <select class="form-control" name="xboxRegion">
				<option <?php if ($profile['xboxRegion'] === "Americas") echo 'selected="selected"'; ?>>Americas</option>
				<option <?php if ($profile['xboxRegion'] === "Europe") echo 'selected="selected"'; ?>>Europe</option>
				<option <?php if ($profile['xboxRegion'] === "Asia") echo 'selected="selected"'; ?>>Asia</option>
			  </select>
			</div>

			<div class="form-group">
			  <label>XBOX Live compatible multi-player games played</label>
<?php
foreach ($games as $game)
{
	if ($game["gameSystem"] !== 'X360' || $game["gameSystem"] !== 'X1') continue;
?>
			  <div class="checkbox">
				<label>
				  <input type="checkbox" name="games[<?= $game['id'] ?>]" <?php if (in_array($game['id'], $gamesOwned)) echo 'checked="checked"'; ?>> <?= $game['gameName'] ?> (<?= $game['gameSystem'] ?>)
				</label>
			  </div>
<?php 
}
?>
			</div>
			<p>Please <a href="#overview-contact">let me know</a> if there's demand for further XBOX multi-player games to be added here.</p>


			<h4 id="services-nintendo">Nintendo</h4>
			<p></p>

			<div class="form-group">
			  <label>Nintendo Switch friend code</label>
			  <input type="text" class="form-control" name="nintendoSwitchFriendCode" value="<?= $profile['nintendoSwitchFriendCode'] ?>" placeholder="Enter Switch friend code">
			  <p class="help-block">Please enter your friend code without the hyphens (ie. 111122224444) and without the "SW-"</p>
			</div>

		   <div class="form-group">
			  <label>Nintendo WiiU name</label>
			  <input type="text" class="form-control" name="nintendoWiiUName" value="<?= $profile['nintendoWiiUName'] ?>" placeholder="Enter WiiU name">
			</div>

			<div class="form-group">
			  <label>Nintendo 3DS friend code</label>
			  <input type="text" class="form-control" name="nintendoFriendCode" value="<?= $profile['nintendoFriendCode'] ?>" placeholder="Enter 3DS friend code">
			  <p class="help-block">Please enter your friend code without the hyphens, e.g.: 111122223333</p>
			</div>

			<div class="form-group">
			  <label>Nintendo compatible multi-player games played</label>
<?php
foreach ($games as $game)
{
	if ($game["gameSystem"] !== 'WiiU' || $game["gameSystem"] !== '3DS' || $game["gameSystem"] !== 'Switch') continue;
?>
			  <div class="checkbox">
				<label>
				  <input type="checkbox" name="games[<?= $game['id'] ?>]" <?php if (in_array($game['id'], $gamesOwned)) echo 'checked="checked"'; ?>> <?= $game['gameName'] ?> (<?= $game['gameSystem'] ?>)
				</label>
			  </div>
<?php
  }
?>
			</div>
			<p>Please <a href="#overview-contact">let me know</a> if there's demand for further Nintendo multi-player games to be added here.</p>

			<h4 id="services-ios">iOS Game Center</h4>
			<p></p>

			<div class="form-group">
			  <label>Game Center profile name</label>
			  <input type="text" class="form-control" name="iosName" value="<?= $profile['iosName'] ?>" placeholder="Enter profile name">
			</div>


			<h3 id="games" class="page-header">Games</h3>

			<h4 id="games-lol">League of Legends</h4>
			<p></p>

			<div class="form-group">
			  <label>Summoner name</label>
			  <input type="text" class="form-control" name="lolName" value="<?= $profile['lolName'] ?>" placeholder="Enter summoner name">
			  <p class="help-block">Your in-game name.</p>
			</div>

			<div class="form-group">
			  <label>Region</label>
			  <select class="form-control" name="lolRegion">
				<option <?php if ($profile['lolRegion'] === "North America") echo 'selected="selected"'; ?>>North America</option>
				<option <?php if ($profile['lolRegion'] === "EU West") echo 'selected="selected"'; ?>>EU West</option>
				<option <?php if ($profile['lolRegion'] === "EU Nordic") echo 'selected="selected"'; ?>>EU Nordic</option>
				<option <?php if ($profile['lolRegion'] === "Oceania") echo 'selected="selected"'; ?>>Oceania</option>
			  </select>
			  <p class="help-block">If your region is missing, please <a href="#overview-contact">let me know</a>. I've only taken the most common ones for the demographic.</p>
			</div>

			<h4 id="games-poe">Path of Exile</h4>
			<p></p>

			<div class="form-group">
			  <label>Path of Exile character name</label>
			  <input type="text" class="form-control" name="poeName" value="<?= $profile['poeName'] ?>" placeholder="Enter character name">
			</div>

			<h4 id="games-pad">Puzzle &amp; Dragons</h4>
			<p></p>

			<div class="form-group">
			  <label>Puzzle &amp; Dragons profile code</label>
			  <input type="text" class="form-control" name="padName" value="<?= $profile['padName'] ?>" placeholder="Enter profile code">
			  <p class="help-block">Please enter your friend code without the separating characters, e.g. 111222333</p>
			</div>

			<h4 id="games-ffrk">FINAL FANTASY Record Keeper</h4>
			<p></p>

			<div class="form-group">
			  <label>Friend ID</label>
			  <input type="text" class="form-control" name="ffrkFriendId" value="<?= $profile['ffrkFriendId'] ?>" placeholder="Enter Friend ID">
			  <p class="help-block">Your Friend ID, found in Menu &gt; Profile</p>
			</div>

			<h3 id="social" class="page-header">Social Networks</h3>
			<p class="lead"></p>

			<div class="alert alert-warning alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			  <strong>Heads up!</strong> If you intend to add your social profiles, you should note that by adding them, you also okay other users of the forum to connect with you on the sites you list. You are under no obligation to provide these, and you can simply leave fields empty to avoid them.
			</div>

			<h4 id="social-ello">Ello</h4>
			<p></p>

			<div class="form-group">
			  <label>Ello name</label>
			  <input type="text" class="form-control" name="elloName" value="<?= $profile['elloName'] ?>" placeholder="Enter your Ello name">
			</div>

			<h4 id="social-facebook">Facebook</h4>
			<p></p>

			<div class="form-group">
			  <label>Facebook profile page</label>
			  <input type="text" class="form-control" name="facebookUrl" value="<?= $profile['facebookUrl'] ?>" placeholder="Enter Facebook profile page address">
			  <p class="help-block">To get to your profile page to retrieve the address, click your name in the upper left corner of the Facebook frontpage, then paste the contents of the address bar here.</p>
			</div>

			<h4 id="social-gplus">Google+</h4>
			<p></p>

			<div class="form-group">
			  <label>Google+ profile page</label>
			  <input type="text" class="form-control" name="googlePlusUrl" value="<?= $profile['googlePlusUrl'] ?>" placeholder="Enter Google+ profile page address">
			  <p class="help-block">To get to your profile page to retrieve the address, click the "Home" dropdown menu in the top-right, and select "Profile."</p>
			</div>

			<h4 id="social-linkedin">LinkedIn</h4>
			<p></p>

			<div class="form-group">
			  <label>LinkedIn profile page</label>
			  <input type="text" class="form-control" name="linkedInUrl" value="<?= $profile['linkedInUrl'] ?>" placeholder="Enter LinkedIn profile page address">
			  <p class="help-block">To get to your profile page to retrieve the address, click the "Profile" link in the top navigation menu.</p>
			</div>

			<h4 id="social-twitter">Twitter</h4>
			<p></p>

			<div class="form-group">
			  <label>Twitter handle</label>
			  <input type="text" class="form-control" name="twitterName" value="<?= $profile['twitterName'] ?>" placeholder="Enter Twitter handle">
			  <p class="help-block">Enter your handle without the '@', please.</p>
			</div>

			<h4 id="social-twitch">Twitch</h4>
			<p></p>

			<div class="form-group">
			  <label>Twitch channel name</label>
			  <input type="text" class="form-control" name="twitchName" value="<?= $profile['twitchName'] ?>" placeholder="Enter Twitch channel name">
			</div>
			<p>Please see this <a href="https://abandonedjerks.com/topic/385-twitch-streaming/">thread</a> for information on the Abandoned Jerks group, and information on how to get added to it.</p>

			<h3 id="social-youtube">YouTube</h3>
			<p></p>

			<div class="form-group">
			  <label>YouTube channel name</label>
			  <input type="text" class="form-control" name="youtubeName" placeholder="Enter YouTube channel name">
			  <p class="help-block">You only need to enter the channel name. The link will be generated with that, e.g.: "https://www.youtube.com/{yournamehere}"</p>
			</div>

		  </form> <!-- /form -->
		</div> <!-- /main -->


		<nav class="col-md-3 rhs-menu">
		  <ul class="nav nav-stacked fixed">
			<li>
			  <a href="#overview">Overview</a>
			  <ul class="nav nav-stacked">
				<li><a href="#overview-forminfo">Mostly optional</a></li>
				<li><a href="#overview-datausage">Data usage</a></li>
				<li><a href="#overview-contact"><i class="fa fa-envelope-square"></i> Contact</a></li>
			  </ul>
			</li>
			<li>
			  <a href="#profile">Your Profile</a>
			</li>
			<li>
			  <a href="#platforms">Platforms</a>
			  <ul class="nav nav-stacked">
				<li><a href="#platforms-steam"><i class="fa fa-steam-square"></i> Steam</a></li>
				<li><a href="#platforms-origin">Origin</a></li>
			  </ul>
			</li>
			<li>
			  <a href="#services">Game Services</a>
			  <ul class="nav nav-stacked">
				<li><a href="#services-bnet">Battle.net</a></li>
				<li><a href="#services-psn">PlayStation Network</a></li>
				<li><a href="#services-xbox">XBOX Live</a></li>
				<li><a href="#services-nintendo">Nintendo</a></li>
				<li><a href="#services-ios">iOS Game Center</a></li>
			  </ul>
			</li>
			<li>
			  <a href="#games">Specific Games</a>
			  <ul class="nav nav-stacked">
				<li><a href="#games-lol">League of Legends</a></li>
				<li><a href="#games-pad">Puzzle &amp; Dragons</a></li>
				<li><a href="#games-poe">Path of Exile</a></li>
			  </ul>
			</li>
			<li>
			  <a href="#social">Social Networks</a>
			  <ul class="nav nav-stacked">
				<li><a href="#social-ello">Ello</a></li>
				<li><a href="#social-facebook"><i class="fa fa-facebook-square"></i> Facebook</a></li>
				<li><a href="#social-gplus"><i class="fa fa-google-plus-square"></i> Google+</a></li>
				<li><a href="#social-linkedin"><i class="fa fa-linkedin-square"></i> LinkedIn</a></li>
				<li><a href="#social-twitter"><i class="fa fa-twitter-square"></i> Twitter</a></li>
				<li><a href="#social-twitch">Twitch</a></li>
				<li><a href="#social-youtube">YouTube</a></li>
			  </ul>
			</li>
			<li class="divider"></li>
			<li>
			  <button id="btnSaveProfile" type="button" class="btn btn-primary btn-block" style="margin-top:20px;">Save Changes</button>
			</li>
		  </ul>
		</nav>

	  </div> <!-- /row -->

	</div> <!-- /container -->


	<script>
	  $('body').scrollspy({
		target: '.rhs-menu',
		offset: 75
	  });

	  $('#btnSaveProfile').click(function () {
		$('#formUpdateProfile').submit();
	  });
	</script>