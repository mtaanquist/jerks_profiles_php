<div class="container">
	<div class="row">
        <div class="col-md-6">
			<h3 class="page-header">Update Existing Profile</h3>
			<p>Use this option if you already have a profile created. Use your profile name (equal to your forum name) and the passphrase you set or were given.</p>
			<form role="form" method="post" action="/profiles/signin">
				<div class="form-group">
					<label>Profile name</label>
					<input type="text" class="form-control" name="profileName" placeholder="Enter profile name" />
				</div>
				<div class="form-group">
					<label>Profile passphrase</label>
					<input type="password" class="form-control" name="profilePassphrase" placeholder="Enter profile passphrase" />
				</div>
				<button type="submit" class="btn btn-primary">Update Your Profile</button>
			</form>
		</div>
		<div class="col-md-6">
			<h3 class="page-header">Create New Profile</h3>
			<p>If you don't already have a profile created, please use this option.</p>
			<p><a href="/profiles/create/" class="btn btn-default">Create Your Profile</a></p>
        </div>
	</div>
</div>