<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2 class="page-header"><?= $profile['profileName'] ?> <small>Created <?= date('Y/m/d', strtotime($profile['profileCreated'])) ?>, last updated at <?= date('Y/m/d', strtotime($profile['profileLastUpdated'])) ?></small></h2>
            <p>You are currently viewing an individual profile. To view a list of all profiles, please go back to <a href="/">the list</a>.</p>
        </div>     
    </div>
    <!-- Platforms -->
<?php 
if (!empty($profile['steamName']) || !empty($profile['originName'])) {
?>
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-header">Platforms</h3>
        </div>
<?php 
if (!empty($profile['steamName'])) { 
?>
        <div class="col-md-3">
            <h4><i class="fa fa-steam"></i> Steam</h4>
            <p>
                <?= $profile['steamName'] ?>
<?php 
if (!empty($profile['steamUrl'])) { 
?>
                (<a href="<?= $profile['steamUrl']?>">Community profile</a>)
<?php 
} 
?>
            </p>
        </div>
<?php 
} 

if (!empty($profile['originName'])) { 
?>
        <div class="col-md-3">
            <h4>Origin</h4>
            <p><?= $profile['originName'] ?></p>
        </div>
<?php 
} 
?>
    </div>
<?php
}
?>
    <!-- Services -->
<?php 
if (!empty($profile['bnetName']) || !empty($profile['psnName']) || !empty($profile['xboxName']) || !empty($profile['nintendoWiiUName']) || !empty($profile['nintendoFriendCode']) || !empty($profile['iosName'])) {
?>
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-header">Services</h3>
        </div>
<?php 
if (!empty($profile['bnetName'])) { 
?>
        <div class="col-md-3">
            <h4>Battle.net</h4>
            <p><?= $profile['bnetName'] ?> <small>(<?= $profile['bnetRegion'] ?>)</small></p>
        </div>
<?php 
} 
?>
<?php
if (!empty($profile['psnName'])) { 
?>
        <div class="col-md-3">
            <h4>PlayStation Network</h4>
            <p><?= $profile['psnName'] ?> <small>(<?= $profile['psnRegion'] ?>)</small></p>
        </div>
<?php } ?>
<?php if (!empty($profile['xboxName'])) { ?>
        <div class="col-md-3">
            <h4>XBOX Live</h4>
            <p><?= $profile['xboxName'] ?> <small>(<?= $profile['xboxRegion'] ?>)</small></p>
        </div>
<?php } ?>
<?php if (!empty($profile['nintendoSwitchFriendCode'])) { ?>
        <div class="col-md-3">
            <h4>Nintendo Switch</h4>
            <p><?= $profile['nintendoSwitchFriendCode'] ?></p>
        </div>
<?php } ?>
<?php if (!empty($profile['nintendoWiiUName'])) { ?>
        <div class="col-md-3">
            <h4>Nintendo WiiU</h4>
            <p><?= $profile['nintendoWiiUName'] ?></p>
        </div>
<?php } ?>
<?php if (!empty($profile['nintendoFriendCode'])) { ?>
        <div class="col-md-3">
            <h4>Nintendo 3DS</h4>
            <p><?= $profile['nintendoFriendCode'] ?></p>
        </div>
<?php } ?>
<?php if (!empty($profile['iosName'])) { ?>
        <div class="col-md-3">
            <h4>iOS Game Center</h4>
            <p><?= $profile['iosName'] ?></p>
        </div>
<?php } ?>
    </div>
<?php } ?>

    <!-- Social networks -->
<?php 
    if (!empty($profile['elloName']) || !empty($profile['facebookUrl']) || !empty($profile['googlePlusUrl']) || !empty($profile['linkedInUrl']) || !empty($profile['twitterName']) || !empty($profile['twitchName']))
    {
?>
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-header">Social Networks</h3>
        </div>
<?php if (!empty($profile['elloName'])) { ?>
        <div class="col-md-3">
            <h4>Ello</h4>
            <p><?= $profile['elloName'] ?> (<a href="https://ello.co/<?= $profile['elloName'] ?>">Ello.co</a>)</p>
        </div>
<?php } ?>
<?php if (!empty($profile['facebookUrl'])) { ?>
        <div class="col-md-3">
            <h4><i class="fa fa-facebook"></i> Facebook</h4>
            <p><a href="<?= $profile['facebookUrl'] ?>">Profile</a></p>
        </div>
<?php } ?>
<?php if (!empty($profile['googlePlusUrl'])) { ?>
        <div class="col-md-3">
            <h4><i class="fa fa-google-plus"></i> Google+</h4>
            <p><a href="<?= $profile['googlePlusUrl'] ?>">Profile</a></p>
        </div>
<?php } ?>
<?php if (!empty($profile['linkedInUrl'])) { ?>
        <div class="col-md-3">
            <h4><i class="fa fa-linkedin"></i> LinkedIn</h4>
            <p><a href="<?= $profile['linkedInUrl'] ?>">Profile</a></p>
        </div>
<?php } ?>
<?php if (!empty($profile['twitterName'])) { ?>
        <div class="col-md-3">
            <h4><i class="fa fa-twitter"></i> Twitter</h4>
            <p>@<?= $profile['twitterName'] ?> (<a href="https://twitter.com/<?= $profile['twitterName'] ?>/">Twitter</a>)</p>
        </div>
<?php } ?>
<?php if (!empty($profile['twitchName'])) { ?>
        <div class="col-md-3">
            <h4><i class="fa fa-twitch"></i> Twitch</h4>
            <p><?= $profile['twitchName'] ?> (<a href="http://www.twitch.tv/<?= $profile['twitchName'] ?>/">Twitch.tv</a>)</p>
        </div>
<?php } ?>
<?php if (!empty($profile['youtubeName'])) { ?>
        <div class="col-md-3">
            <h4><i class="fa fa-youtube"></i> YouTube</h4>
            <p><?= $profile['youtubeName'] ?> (<a href="https://www.youtube.com/<?= $profile['youtubeName'] ?>/">YouTube</a>)</p>
        </div>
<?php } ?>
    </div>
<?php
}
if (!empty($ownedGames))
{
?>

    <div class="row">
        <div class="col-md-12">
            <h3 class="page-header">Games owned</h3>
            <p>Listed below are all games owned by <?= $profile['profileName'] ?>, across all platforms and services.</p>
            <div class="row">
<?php

foreach ($ownedGames as $ownedGame) {
?>
                <div class="col-md-3">
                    <p>
                        <a href="/games/details/<?= $ownedGame['id'] ?>" class="btn btn-link btn-block"><?= $ownedGame['gameName'] ?></a>
                    </p>
                </div>
<?php
}
?>
            </div>
        </div>
    </div>
<?php 
}
?>
</div>
