<div class="container">

	<div class="row">
		<div class="col-md-9" role="main">
			<h1 id="overview" class="page-header">Overview</h1>
			<p class="lead">A quick rundown of the form, how the site uses your data, and contact information should you have any further questions.</p>
			
			<h3 id="overview-forminfo">Mostly optional</h3>
			<p>This form is fairly lengthy, which is why the navigation menu to the right exists. There are very few fields that are required, and as such can be left blank if you'd like to.</p>

			<h3 id="overview-datausage">Data usage</h3>
			<p>Your data is only used for display in the table on this page/application. It's not being sold or traded, nor is tracking information employed. Normal Internet etiquette should always be considered, though, so don't share anything you don't want people to find out.</p>

			<h3 id="overview-contact">Contact</h3>
			<p>Should you have any questions regarding this form (or the page), please don't hesitate in shooting me an <a href="mailto:jerks@karantor.com">e-mail</a>. You should receive a reply within 24 hours.</p>

			<form id="formNewProfile" method="post" action="/profiles/new" role="form">
				<h1 id="profile" class="page-header">Your Profile</h1>
				<p class="lead"></p>

				<div class="form-group">
					<label>Forum username (required)</label>
					<input type="text" class="form-control" name="profileName" placeholder="Enter forum username" required>
					<p class="help-block">The purpose of this site is to link your forum username to your other gaming or social profiles, and as such it is imperative that you use the same username here as you do on Abandoned Jerks.</p>
				</div>

				<div class="form-group">
					<label>Profile passphrase</label>
					<input type="text" class="form-control" name="profilePassphrase" placeholder="Provide a passphrase">
					<p class="help-block">The passphrase is used to update your profile, and is optional to provide. Should you choose not to provide one, the system will auto-generate one for you, which will be shown after creating the profile. This is a chance to customise it yourself. Do NOT use a password you're using elsewhere, as this is stored in clear text.</p>
				</div>


				<h1 id="platforms" class="page-header">Platforms</h1>
				<p class="lead"></p>

				<div class="alert alert-info alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<strong>Note!</strong> When asked which games you own, this is to help others - who also play that game - find people to play with. You should consider not ticking a game as owned if you've never played it, or never intend to again. You can always change it later!
					
				</div>

				<h3 id="platforms-steam">Steam</h3>
				<p>Both your Steam name and Steam profile page link address can be changed by yourself, so make sure that you check the information in your Steam client before saving your profile.</p>
				
				<div class="form-group">
					<label>Steam name</label>
					<input type="text" class="form-control" name="steamName" placeholder="Enter Steam name">
					<p class="help-block">Your Steam name is the name that is shown in games, or when you're talking with people from the Friends List.</p>
				</div>

				<div class="form-group">
					<label>Steam profile page</label>
					<input type="text" class="form-control" name="steamUrl" placeholder="Enter Steam profile page address">
					<p class="help-block">You can find your profile page address by hovering over your name in the top navigation bar within the Steam client, then selecting "Profile." Once the page has loaded, right-click in a blank space and select "Copy Page URL."</p>
				</div>

				<div class="form-group">
					<label>Valve multi-player games played</label>
<?php
foreach ($games as $game)
{
	if ($game["gameSystem"] !== 'Steam') continue;
?>
					<div class="checkbox">
						<label>
							<input type="checkbox" name="games[<?= $game['id'] ?>]"> <?= $game['gameName'] ?>
						</label>
					</div>
<?php
}
?>
				</div>

				<p>Please <a href="#overview-contact">let me know</a> if there's demand for further Valve multi-player games to be added here.</p>


				<h3 id="platforms-origin">Origin</h3>
				<p></p>

				<div class="form-group">
					<label>Origin ID</label>
					<input type="text" class="form-control" name="originName" placeholder="Enter Origin ID">
					<p class="help-block">Your Origin ID is the name shown in the upper-right corner of the Origin client.</p>
				</div>

				<div class="form-group">
					<label>EA multi-player games played</label>
<?php
foreach ($games as $game)
{
	if ($game["gameSystem"] !== 'Origin') continue;
?>
					<div class="checkbox">
						<label>
							<input type="checkbox" name="games[<?= $game['id'] ?>]"> <?= $game['gameName'] ?>
						</label>
					</div>
<?php
}
?>
				</div>

				<p>Please <a href="#overview-contact">let me know</a> if there's demand for further EA multi-player games to be added here.</p>

				<h1 id="services" class="page-header">Services</h1>
				<p class="lead"></p>

				<div class="alert alert-info alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<strong>Note!</strong> When asked which games you own, this is to help others - who also play that game - find people to play with. You should consider not ticking a game as owned if you've never played it, or never intend to again. You can always change it later!
				</div>

				<h3 id="services-bnet">Battle.net</h3>
				<p></p>

				<div class="form-group">
					<label>BattleTag</label>
					<input type="text" class="form-control" name="bnetName" placeholder="Enter BattleTag">
					<p class="help-block">Include both the name and identifier, e.g.: "SomeName#1234"</p>
				</div>

				<div class="form-group">
					<label>Primary region</label>
					<select class="form-control" name="bnetRegion">
						<option>Americas</option>
						<option>Europe</option>
						<option>Asia</option>
					</select>
					<p class="help-block">Battle.net allows you to log into most regions, but which would you consider your primary region?</p>
				</div>

				<div class="form-group">
					<label>Battle.net compatible multi-player games played</label>
<?php
foreach ($games as $game)
{
	if ($game["gameSystem"] !== 'Blizzard') continue;
?>
					<div class="checkbox">
						<label>
							<input type="checkbox" name="games[<?= $game['id'] ?>]"> <?= $game['gameName'] ?>
						</label>
					</div>
<?php
}
?>
				</div>

				<p>Please <a href="#overview-contact">let me know</a> if there's demand for further Battle.net/Blizzard multi-player games to be added here.</p>

				<h3 id="services-psn">PlayStation Network</h3>
				<p></p>

				<div class="form-group">
					<label>PSN ID</label>
					<input type="text" class="form-control" name="psnName" placeholder="Enter PSN ID">
				</div>

				<div class="form-group">
					<label>Primary region</label>
					<select class="form-control" name="psnRegion">
						<option>Americas</option>
						<option>Europe</option>
						<option>Asia</option>
					</select>
				</div>

				<div class="form-group">
					<label>PlayStation Network compatible multi-player games played</label>
<?php
foreach ($games as $game)
{
	if ($game["gameSystem"] !== 'PS3' || $game["gameSystem"] !== 'PS4') continue;
?>
					<div class="checkbox">
						<label>
							<input type="checkbox" name="games[<?= $game['id'] ?>]"> <?= $game['gameName'] ?> (<?= $game['gameSystem'] ?>)
						</label>
					</div>
<?php
}
?>
				</div>
				<p>Please <a href="#overview-contact">let me know</a> if there's demand for further PlayStation Network multi-player games to be added here.</p>				 
				
				<h3 id="services-xbox">XBOX Live</h3>
				<p></p>

				<div class="form-group">
					<label>XBOX Live Name</label>
					<input type="text" class="form-control" name="xboxName" placeholder="Enter XBOX Live name">
				</div>

				<div class="form-group">
					<label>Primary region</label>
					<select class="form-control" name="xboxRegion">
						<option>Americas</option>
						<option>Europe</option>
						<option>Asia</option>
					</select>
				</div>

				<div class="form-group">
					<label>XBOX Live compatible multi-player games played</label>
<?php
foreach ($games as $game)
{
	if ($game["gameSystem"] !== 'X360' || $game["gameSystem"] !== 'X1') continue;
?>
					<div class="checkbox">
						<label>
							<input type="checkbox" name="games[<?= $game['id'] ?>]"> <?= $game['gameName'] ?> (<?= $game['gameSystem'] ?>)
						</label>
					</div>
<?php
}
?>
				</div>
				<p>Please <a href="#overview-contact">let me know</a> if there's demand for further XBOX multi-player games to be added here.</p>


				<h3 id="services-nintendo">Nintendo</h3>
				<p></p>

				<div class="form-group">
					<label>Nintendo Switch friend code</label>
					<input type="text" class="form-control" name="nintendoSwitchFriendCode" placeholder="Enter Switch friend code">
					<p class="help-block">Please enter your friend code without the hyphens, e.g.: 111122223333</p>
				</div>							

				<div class="form-group">
					<label>Nintendo WiiU name</label>
					<input type="text" class="form-control" name="nintendoWiiUName" placeholder="Enter WiiU name">
				</div>

				<div class="form-group">
					<label>Nintendo 3DS friend code</label>
					<input type="text" class="form-control" name="nintendoFriendCode" placeholder="Enter 3DS friend code">
					<p class="help-block">Please enter your friend code without the hyphens, e.g.: 111122223333</p>
				</div>					

				<div class="form-group">
					<label>Nintendo compatible multi-player games played</label>
<?php
foreach ($games as $game)
{
	if ($game["gameSystem"] !== 'WiiU' || $game["gameSystem"] !== '3DS' || $game["gameSystem"] !== 'Switch') continue;
?>
					<div class="checkbox">
						<label>
							<input type="checkbox" name="games[<?= $game['id'] ?>]"> <?= $game['gameName'] ?> (<?= $game['gameSystem'] ?>)
						</label>
					</div>
<?php
}
?>
				</div>
				<p>Please <a href="#overview-contact">let me know</a> if there's demand for further Nintendo multi-player games to be added here.</p>

				<h3 id="services-ios">iOS Game Center</h3>
				<p></p>

				<div class="form-group">
					<label>Game Center profile name</label>
					<input type="text" class="form-control" name="iosName" placeholder="Enter profile name">
				</div>


				<h1 id="games" class="page-header">Games</h1>
				<div class="alert alert-info alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<strong>Note!</strong> When asked which games you own, this is to help others - who also play that game - find people to play with. You should consider not ticking a game as owned if you've never played it, or never intend to again. You can always change it later!
				</div>

				<h3 id="games-lol">League of Legends</h3>
				<p></p>

				<div class="form-group">
					<label>Summoner name</label>
					<input type="text" class="form-control" name="lolName" placeholder="Enter summoner name">
					<p class="help-block">Your in-game name.</p>
				</div>

				<div class="form-group">
					<label>Region</label>
					<select class="form-control" name="lolRegion">
						<option>North America</option>
						<option>EU West</option>
						<option>EU Nordic &amp; East</option>
						<option>Brazil</option>
						<option>Latin America North</option>
						<option>Latin America South</option>
						<option>Oceania</option>
						<option>Russia</option>
						<option>Turkey</option>
						<option>Republic of Korea</option>
					</select>
				</div>

				<h3 id="games-poe">Path of Exile</h3>
				<p></p>

				<div class="form-group">
					<label>Path of Exile character name</label>
					<input type="text" class="form-control" name="poeName" placeholder="Enter character name">
				</div>

				<h3 id="games-pad">Puzzle &amp; Dragons</h3>
				<p></p>

				<div class="form-group">
					<label>Puzzle &amp; Dragons profile code</label>
					<input type="text" class="form-control" name="padName" placeholder="Enter profile code">
					<p class="help-block">Please enter your friend code without the separating characters, e.g. 111222333</p>
				</div>

				<h3 id="games-pad">FINAL FANTASY Record Keeper</h3>
				<p></p>

				<div class="form-group">
					<label>FINAL FANTASY Record Keeper Friend ID</label>
					<input type="text" class="form-control" name="ffrkFriendId" placeholder="Enter Friend ID">
					<p class="help-block">Your Friend ID, found in Menu &gt; Profile</p>
				</div>

				<h1 id="social" class="page-header">Social Networks</h1>
				<p class="lead"></p>

				<div class="alert alert-warning alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<strong>Heads up!</strong> If you intend to add your social profiles, you should note that by adding them, you also okay other users of the forum to connect with you on the sites you list. You are under no obligation to provide these, and you can simply leave fields empty to avoid them.
				</div>

				<h3 id="social-ello">Ello</h3>
				<p></p>

				<div class="form-group">
					<label>Ello name</label>
					<input type="text" class="form-control" name="elloName" placeholder="Enter your Ello name">
				</div>

				<h3 id="social-facebook">Facebook</h3>
				<p></p>

				<div class="form-group">
					<label>Facebook profile page</label>
					<input type="text" class="form-control" name="facebookUrl" placeholder="Enter Facebook profile page address">
					<p class="help-block">To get to your profile page to retrieve the address, click your name in the upper left corner of the Facebook frontpage, then paste the contents of the address bar here.</p>
				</div>

				<h3 id="social-gplus">Google+</h3>
				<p></p>

				<div class="form-group">
					<label>Google+ profile page</label>
					<input type="text" class="form-control" name="googlePlusUrl" placeholder="Enter Google+ profile page address">
					<p class="help-block">To get to your profile page to retrieve the address, click the "Home" dropdown menu in the top-right, and select "Profile."</p>
				</div>

				<h3 id="social-linkedin">LinkedIn</h3>
				<p></p>

				<div class="form-group">
					<label>LinkedIn profile page</label>
					<input type="text" class="form-control" name="linkedInUrl" placeholder="Enter LinkedIn profile page address">
					<p class="help-block">To get to your profile page to retrieve the address, click the "Profile" link in the top navigation menu.</p>
				</div>

				<h3 id="social-twitter">Twitter</h3>
				<p></p>

				<div class="form-group">
					<label>Twitter handle</label>
					<input type="text" class="form-control" name="twitterName" placeholder="Enter Twitter handle">
					<p class="help-block">Enter your handle without the '@', please.</p>
				</div>

				<h3 id="social-twitch">Twitch</h3>
				<p></p>

				<div class="form-group">
					<label>Twitch channel name</label>
					<input type="text" class="form-control" name="twitchName" placeholder="Enter Twitch channel name">
				</div>
				<p>Please see this <a href="https://abandonedjerks.com/topic/385-twitch-streaming/">thread</a> for information on the Abandoned Jerks group, and information on how to get added to it.</p>

				<h3 id="social-youtube">YouTube</h3>
				<p></p>

				<div class="form-group">
					<label>YouTube channel name</label>
					<input type="text" class="form-control" name="youtubeName" placeholder="Enter YouTube channel name">
					<p class="help-block">You only need to enter the channel name. The link will be generated with that, e.g.: "https://www.youtube.com/{yournamehere}"</p>
				</div>

			</form> <!-- /form -->
		</div> <!-- /main -->


		<nav class="col-md-3 rhs-menu">
			<ul class="nav nav-stacked fixed">
				<li>
					<a href="#overview">Overview</a>
					<ul class="nav nav-stacked">
						<li><a href="#overview-forminfo">Mostly optional</a></li>
						<li><a href="#overview-datausage">Data usage</a></li>
						<li><a href="#overview-contact"><i class="fa fa-envelope-square"></i> Contact</a></li>
					</ul>
				</li>
				<li>
					<a href="#profile">Your Profile</a>
				</li>
				<li>
					<a href="#platforms">Platforms</a>
					<ul class="nav nav-stacked">
						<li><a href="#platforms-steam"><i class="fa fa-steam-square"></i> Steam</a></li>
						<li><a href="#platforms-origin">Origin</a></li>
					</ul>
				</li>
				<li>
					<a href="#services">Game Services</a>
					<ul class="nav nav-stacked">
						<li><a href="#services-bnet">Battle.net</a></li>
						<li><a href="#services-psn">PlayStation Network</a></li>
						<li><a href="#services-xbox">XBOX Live</a></li>
						<li><a href="#services-nintendo">Nintendo</a></li>
						<li><a href="#services-ios">iOS Game Center</a></li>
					</ul>
				</li>
				<li>
					<a href="#games">Specific Games</a>
					<ul class="nav nav-stacked">
						<li><a href="#games-lol">League of Legends</a></li>
						<li><a href="#games-pad">Puzzle &amp; Dragons</a></li>
						<li><a href="#games-poe">Path of Exile</a></li>
					</ul>
				</li>
				<li>
					<a href="#social">Social Networks</a>
					<ul class="nav nav-stacked">
						<li><a href="#social-ello">Ello</a></li>
						<li><a href="#social-facebook"><i class="fa fa-facebook-square"></i> Facebook</a></li>
						<li><a href="#social-gplus"><i class="fa fa-google-plus-square"></i> Google+</a></li>
						<li><a href="#social-linkedin"><i class="fa fa-linkedin-square"></i> LinkedIn</a></li>
						<li><a href="#social-twitter"><i class="fa fa-twitter-square"></i> Twitter</a></li>
						<li><a href="#social-twitch">Twitch</a></li>
						<li><a href="#social-youtube">YouTube</a></li>
					</ul>
				</li>
				<li class="divider"></li>
				<li>
					<button id="btnSaveProfile" type="button" class="btn btn-primary btn-block" style="margin-top:20px;">Save Profile</button>
				</li>
			</ul>
		</nav>

	</div> <!-- /row -->

</div> <!-- /container -->


<script>
	$('body').scrollspy({
		target: '.rhs-menu',
		offset: 75
	});

	$('#btnSaveProfile').click(function () {
		$('#formNewProfile').submit();
	});
</script>
