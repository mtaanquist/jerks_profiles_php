<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Gaming/social profile database">
    <meta name="author" content="Karantor">

    <title>Abandoned Jerks - Profiles</title>

    <link href="/app/assets/css/bootstrap.sandstone.min.css" rel="stylesheet">
    <link href="/app/assets/css/bootstrap-override.css" rel="stylesheet">
    <link href="/app/assets/css/font-awesome.min.css" rel="stylesheet">

    <link rel="icon" href="/favicon.ico" type="image/x-icon" />

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="/app/assets/js/libs/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="/app/assets/js/libs/jquery-3.2.1.js"></script>
    <script src="/app/assets/js/libs/bootstrap.js"></script>
    <script src="/app/assets/js/libs/typeahead.bundle.js"></script>
  </head>

  <body>

    <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">AJ Profiles</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li <?php if ($page == "list") echo 'class="active"'; ?>><a href="/">Profiles</a></li>
            <li <?php if ($page == "games") echo 'class="active"'; ?>><a href="/games/">Games</a></li>
            <!-- <li <?php if ($page == "groups") echo 'class="active"'; ?>><a href="/groups/">Groups</a></li> -->
            <!-- <li <?php if ($page == "streams") echo 'class="active"'; ?>><a href="/streams/">Streams</a></li> -->
            <li <?php if ($page == "update") echo 'class="active"'; ?>><a href="/profiles/">My Profile</a></li>
            <li <?php if ($page == "extension") echo 'class="active"'; ?>><a href="/extension/">Extension</a></li>
          </ul>
          <!--
          <form class="navbar-form navbar-right" role="form" method="post">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Search for profile NYI WIP">
              <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          -->
        </div><!--/.nav-collapse -->
      </div>
    </div>