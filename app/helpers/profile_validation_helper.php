<?php
namespace App\Helpers;

class ProfileValidationHelper 
{
    private $_expectations;

    function __construct()
    {
        $this->$_expectations = [
            'profileName' => [
                'type' => 'string',
                'regex' => '/^[\.\p{L}0-9_-]{3,32}$/iu',
            ],
            'profilePassphrase' => [
                'type' => 'string',
                'regex' => '/^.{3,250}$/iu',
            ],
            'steamName' => [
                'type' => 'string',
                'regex' => '/.{1,250}$/iu',
            ],
            'steamUrl' => [
                'type' => 'string',
                'regex' => '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/iu',
            ],
            'originName' => [
                'type' => 'string',
                'regex' => '/^[\.a-z0-9_-]{3,32}$/iu',
            ],
            'bnetName' => [
                'type' => 'string',
                'regex' => '/^[\w0-9]+#[0-9]{4,5}/iu',
            ],
            'psnName' => [
                'type' => 'string',
                'regex' => '/^[\.\p{L}0-9_-]{3,32}$/iu',
            ],
            'xboxName' => [
                'type' => 'string',
                'regex' => '/^[\s\.\p{L}0-9_-]{3,32}$/iu',
            ],
            'nintendoSwitchFriendCode' => [
                'type' => 'string',
                'regex' => '/^[0-9]{12}/',
            ],
            'nintendoWiiUName' => [
                'type' => 'string',
                'regex' => '/^[\p{L}0-9_-]{3,32}$/iu',
            ],
            'nintendoFriendCode' => [
                'type' => 'string',
                'regex' => '/^[0-9]{12}/',
            ],
            'iosName' => [
                'type' => 'string',
                'regex' => '/^[\.a-z0-9_-]{3,32}$/iu',
            ], 
            'lolName' => [
                'type' => 'string',
                'regex' => '/^[\p{L}0-9_-]{3,32}$/iu',
            ],
            'padName' => [
                'type' => 'string',
                'regex' => '/^[0-9]{9}/',
            ],
            'poeName' => [
                'type' => 'string',
                'regex' => '/^[\.\p{L}0-9_-]{3,250}$/iu',
            ],
            'elloName' => [
                'type' => 'string',
                'regex' => '/^[\p{L}0-9_-]{3,32}$/iu',
            ],
            'facebookUrl' => [
                'type' => 'string',
                'regex' => '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\S\w \.-]*)*\/?$/iu',
            ],
            'googlePlusUrl' => [
                'type' => 'string',
                'regex' => '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\S\w \.-]*)*\/?$/iu',
            ],
            'linkedInUrl' => [
                'type' => 'string',
                'regex' => '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\S\w \.-]*)*\/?$/iu',
            ],
            'twitterName' => [
                'type' => 'string',
                'regex' => '/^[\p{L}0-9_-]{3,32}$/iu',
            ],
            'twitchName' => [
                'type' => 'string',
                'regex' => '/^[\p{L}0-9_-]{3,32}$/iu',
            ],
            'youtubeName' => [
                'type' => 'string',
                'regex' => '/^[\p{L}0-9_-]{3,32}$/iu',
            ],
            'ffrkFriendId' => [
                'type' => 'string',
                'regex' => '/^[\.\p{L}0-9_-]{3,32}$/iu',
            ],
        ];        
    }

    function validateFields($fields) 
    {
        foreach ($fields as $key => $value) {
            // skip some fields
            $skipFields = [
                'bnetRegion', 'psnRegion', 'xboxRegion', 'lolRegion', 'games'
            ];
            
            if (in_array($key, $skipFields) || empty($value)) {
                continue;
            }

            if (preg_match($this->$_expectations["$key"]["regex"], $value) == 0) {
                $message = 'Failed to validate given input for field "'. $key .
                            '." The value given was: '. $value;
                return $message;
            }
        }

        return true;
    }    
}

?>
