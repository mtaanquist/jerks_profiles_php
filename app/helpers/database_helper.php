<?php
namespace App\Helpers;

class DatabaseHelper
{
    private $_mysqli;
    
    public function __construct($host, $username, $password, $database) 
    {
        if (DB_SSL) {
            $this->_mysqli = mysqli_init();
            $flags = MYSQLI_CLIENT_SSL_DONT_VERIFY_SERVER_CERT;
            mysqli_ssl_set($this->_mysqli, NULL, NULL, DB_SSL_CERT, NULL, NULL);
            mysqli_real_connect($this->_mysqli, DATABASE_HOST, DATABASE_USER, DATABASE_PASS, DATABASE_NAME, 3306, '', $flags);
        } else {
            $this->_mysqli = new \_mysqli($host, $username, $password, $database);
        }
        
        if ($this->_mysqli->connect_error) {
            throw new \Exception('Database error: ' . $this->_mysqli->connect_error);
        }

        $this->_mysqli->query('SET NAMES utf8');
    }
    
    public function escape($text)
    {
        return $this->_mysqli->real_escape_string($text);
    }
    
    public function getLastId()
    {
        return $this->_mysqli->insert_id;
    }

    public function totalRows()
    {
        $result = $this->_mysqli->query('SELECT FOUND_ROWS()');
        if (!$result) return false;
        $row = $result->fetch_row();
        $result->free();
        return (int)$row[0];
    }	
    
    public function query($queryString)
    {
        $db = $this;
        $replacements = func_get_args();
        array_shift($replacements);
        
        $buildQueryPart = function ($db, $type, $value) {
            if ($value === null) { 
                return 'NULL'; 
            }
            
            switch ($type) {
            case 'b':
                if ($value) return '1';
                return '0';
            case 'd':
                if (!is_numeric($value)) {
                    $value = strtotime($value);
                }
                if ($value == 0) return 'NULL';
                $value = date('Y-m-d H:i:s', $value);
                return '"' . $value . '"';
            case 'f':
                if (!is_numeric($value)) throw new \Exception('Value could not be converted in to a floating point');
                return (string)((float)$value);
            case 'i':
                if (!is_numeric($value)) throw new \Exception('Value could not be converted in to an integer');
                return (string)((int)$value);
            case 'j':
                return '"' . $db->escape(json_encode($value)) . '"';
            case 'r':
                return (string)$value;
            case 's':
                if (is_array($value) || is_object($value)) throw new \Exception('Value could not be converted in to an integer');
                return '"' . $db->escape($value) . '"';
            }
            
            throw new \Exception('Unknown type specifier');
        };
        
        $callback = function($matches) use ($db, &$replacements, $buildQueryPart)
                {
                    if ($matches[1][0] == 'a')
                    {
                        $arrayMode = true;
                        $matches[1] = substr($matches[1], 1);
                    }
                    else
                    {
                        $arrayMode = false;
                    }
                    
                    if ($arrayMode)
                    {
                        foreach (array_shift($replacements) as $value)
                        {
                            $parts[] = $buildQueryPart($db, $matches[1], $value);
                        }
                        return '(' . implode(', ', $parts) . ')';
                    }
                    else
                    {
                        return $buildQueryPart($db, $matches[1], array_shift($replacements));
                    }
                };
        
        $queryString = preg_replace_callback('/%(a?.)/', $callback, $queryString);
        // var_dump($queryString);
        $result = $this->_mysqli->query($queryString);
        if ($result === false) throw new \Exception('Database error: ' . $this->_mysqli->error);
        if ($result === true) return true;
        
        $data = array();
        while ($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
        $result->free();
        return $data;
    }
}

?>