$(function() {
    var igdbLookup = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: 'https://api-2445582011268.apicast.io/games/',
            prepare: function(query, settings) {
                settings.beforeSend = function(jqXHR, settings) {
                    settings.xhrFields = { withCredentials: true };
                };
                settings.url = settings.url + '?search=' + query + '&fields=name';
                settings.headers = {
                    "user-key": "a2ab41f85dda15d3c1eabf6251fe2da0",
                    "Accept": "application/json"
                };
                return settings;
            }
        },
    });

    $('#igdb-lookup .typeahead').typeahead(null, {
        name: 'igdb-lookup',
        display: 'name',
        source: igdbLookup.ttAdapter()
    });
});