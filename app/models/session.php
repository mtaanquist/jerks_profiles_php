<?php
namespace App\Models;

class Session extends \DAL\RecordModel
{
    public $schemaOverride = [
        'table_name' => 'update_token'
    ];

    public $token;
    public $profileId;
}
