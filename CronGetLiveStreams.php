<?php
	require('config.php');
	require('Database.php');
	$db = new Database(DATABASE_HOST, DATABASE_USER, DATABASE_PASS, DATABASE_NAME);

	$channels = $db->query("SELECT profileId, profileName, twitchName FROM profile WHERE twitchName <> ''");

	// before we can insert new live streams, remove the old ones
	$db->query("DELETE FROM live_stream");

	// iterate and find live casters from Twitch API
	$clientId = 'rbyyqhqehavc34o6ukimgm9ixgbrgwd';
	foreach ($channels as $channel) 
	{
		$jsonArray = json_decode(file_get_contents('https://api.twitch.tv/kraken/streams/' . strtolower($channel["twitchName"]) . '?client_id='.$clientId), true);
		if ($jsonArray['stream'] != NULL)
		{
			$sql = "INSERT INTO live_stream (profileId, streamChannelName, streamTitle, streamCurrentGame, streamViewerCount) VALUES (%i, %s, %s, %s, %i)";
			$db->query($sql, 
				$channel['profileId'], 
				$jsonArray['stream']['channel']['display_name'], 
				$jsonArray['stream']['channel']['status'], 
				$jsonArray['stream']['channel']['game'], 
				$jsonArray['stream']['viewers']
			);
		}
	}
?>